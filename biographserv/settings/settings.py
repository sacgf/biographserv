"""
Django settings for biographserv project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import djcelery
import os

from bgs.utils.utils import up_dir

VERSION="1.0"

BIOGRAPHSERV_DIR = up_dir(os.path.dirname(__file__), 1)
BASE_DIR = os.path.dirname(BIOGRAPHSERV_DIR)
print "BASE dir = " + BASE_DIR

SECURE_DATA_DIR = "/etc/biographserv"
if not os.path.isdir(SECURE_DATA_DIR):
    msg = "SECURE_DATA_DIR='%s' does not exist"
    raise ValueError(msg)

def read_secure_file(file_name):
    full_path = os.path.join(SECURE_DATA_DIR, file_name)
    with open(full_path) as f:
        return f.read().strip()

SECRET_KEY = read_secure_file("secret_key.txt")
DB_USER = read_secure_file("database_user.txt")
DB_PASSWORD = read_secure_file("database_password.txt")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True
USE_DJANGO_JQUERY = False
ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 3rd party
    'djcelery',
    'jfu',
    'smart_selects',
    # Mine
    'bgs'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'biographserv.urls'

WSGI_APPLICATION = 'biographserv.wsgi.application'



# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'biographserv',
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': 'localhost',
        'PORT': '',  
    }
}


CACHE_HOURS = 48
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'TIMEOUT': 60 * 60 * CACHE_HOURS,
        'LOCATION': [
            'localhost',
        ]
    }
}

djcelery.setup_loader()
#BROKER_URL = 'amqp://guest:guest@localhost:5672/'
BROKER_URL = 'redis://localhost:6379/0'

CELERY_DEFAULT_QUEUE = 'biographserv'

CELERY_IMPORTS = ("bgs.tasks.bed_file_processor",
                  "bgs.tasks.cuffdiff_processor",
                  "bgs.tasks.expression.expression_processor",
                  "bgs.tasks.vcf_processor.vcf_processor",
)

CELERY_ALWAYS_EAGER = False # True to execute in http server process (or Eclipse)
CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend'
#CELERY_RESULT_BACKEND='cache+memcached://127.0.0.1:11211/'

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

EMAIL_FROM = "BioGraphServ <noreply@biographserv.com>"
EMAIL_HOST = 'localhost'
EMAIL_PORT = 25

LANGUAGE_CODE = 'en-us'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media_root')
if not os.path.exists(MEDIA_ROOT):
    os.mkdir(MEDIA_ROOT)

PROCESSOR_OUTPUT_DIR = os.path.join(MEDIA_ROOT, 'processing')
if not os.path.exists(PROCESSOR_OUTPUT_DIR):
    os.mkdir(PROCESSOR_OUTPUT_DIR)


TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
STATIC_ROOT = os.path.join(BIOGRAPHSERV_DIR, "sitestatic", "static")

STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(BIOGRAPHSERV_DIR, "static_html"),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

TEMPLATE_CONTEXT_PROCESSORS = ( "django.contrib.auth.context_processors.auth",
                                "django.core.context_processors.debug",
                                "django.core.context_processors.i18n",
                                "django.core.context_processors.media",
                                "django.core.context_processors.request",
                                "django.core.context_processors.static",
                                "django.core.context_processors.tz",
                                "django.contrib.messages.context_processors.messages")

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'DEBUG',
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
#         'mail_admins': {
#             'level': 'ERROR',
#             'class': 'django.utils.log.AdminEmailHandler',
#         }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'INFO',
        },
        'bgs': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'DEBUG',
        },

#        'django.request': {
#            'handlers': ['mail_admins'],
#            'level': 'ERROR',
#            'propagate': False,
#        },
    }
}


CHROMOSOME_FORMAT_HAS_CHR = False

IGENOMES_DIR = '/data/sacgf/reference/iGenomes'
REFERENCE_SERVER_HOSTNAME='localhost'
REFERENCE_SERVER_PORT=21993

# Set this to a Unix group for celery/gunicorn/reference_server
GROUP_NAME = None
