from __future__ import absolute_import
import logging
import re
import socket

logger = logging.getLogger(__name__)

# Pull in hostname-based changes.
hostname = socket.gethostname().lower().split('.')[0].replace('-','')
 
# Remove numbers from the end (ie VM copies)
pattern = re.compile("(\d+)$")
hostname = pattern.sub("", hostname)

from biographserv.settings.settings import *

try:
    host_specific_file = 'biographserv.settings.%s' % hostname 
    logger.debug("Trying to import '%s'" % host_specific_file)
    exec "from %s import *" % host_specific_file
    logger.debug("Imported: '%s'" % host_specific_file)
except ImportError as e:
    print e
    # Import normal settings
    print "Normal settings"
    logger.debug("Could not import host specific files (using standard settings only)")
