#!/usr/bin/env python
'''
Created on 01/12/2014

Ideally, I would have just pickled the reference objects and put them in Redis

However - they 

@author: dlawrence
'''

# Must be above models import
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'biographserv.settings' 

from SocketServer import ForkingTCPServer
import SocketServer
from argparse import ArgumentParser
import django
import json
import socket

django.setup()

from bgs import models
from bgs.models import get_processing_dir
from bgs.utils.utils import setup_console_logger
from biographserv import settings
from reference_server.reference.reference import Reference
from reference_server.tasks.process_bed_files import process_bed_file




def handle_args():
    parser = ArgumentParser(description='Reference server')
    parser.add_argument("--build", help='Only load particular reference (default=all in database)')
    parser.add_argument("--log-level", required=False, default='INFO')
    return parser.parse_args()

class ReferenceServer(ForkingTCPServer):
    def __init__(self, *args, **kwargs):
        self.references = kwargs.pop('references')

        # Super is for new style classes only
        ForkingTCPServer.__init__(self, *args, **kwargs)

    def get_reference(self, reference_id):
        try:
            return self.references[reference_id]
        except Exception:
            msg = "Uknown reference %s" % (reference_id)
            raise ValueError(msg)

def process_file(reference, project_id, file_name, file_type, stranded, params):
    PROCESSORS = {'bed' : process_bed_file} 

    try:
        working_dir = get_processing_dir(project_id)
        processor = PROCESSORS[file_type]
        data = processor(reference, working_dir, file_name, stranded, params)
    except Exception as e:
        exception_string = "%s : %s" % (type(e), e.args)
        data = {"error" : exception_string}
    return data


class ReferenceServerRequestHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        data = self.request.recv(2048)
        message = json.loads(data)

        reference_id = message['reference_id']
        project_id = message['project_id']
        file_name = message['file_name']
        file_type = message['file_type']
        stranded = message['stranded']
        params = message.get('params', {})

        reference = self.server.get_reference(reference_id)
        data = process_file(reference, project_id, file_name, file_type, stranded, params)
        self.request.sendall(json.dumps(data))

def client(ip, port, message):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip, port))
    try:
        sock.sendall(message)
        response = sock.recv(2048)
        print "Received: %s" % response
    finally:
        sock.close()

def get_references(load_build):
    references = {}
    for build in models.Build.objects.all():
        if load_build and build.name != load_build:
            print "Skipping build: %s" % build
            continue
        
        igenomes_reference = build.get_igenomes_reference()
        ref_id = igenomes_reference.get_reference_id()
        print "Loading reference: %s" % ref_id
        reference = Reference(igenomes_reference)
        reference.genes # Trigger loading of lazy objects
        references[ref_id] = reference

    return references

if __name__ == "__main__":
    django.setup()

    args = handle_args()
    
    setup_console_logger(log_level=args.log_level)
    
    references = get_references(args.build)
    server = ReferenceServer((settings.REFERENCE_SERVER_HOSTNAME, settings.REFERENCE_SERVER_PORT), ReferenceServerRequestHandler, references=references)
    print "Server %s:%s" % server.server_address
    # Port 0 means to select an arbitrary unused port
    try:
        server.serve_forever()
    except:
        print "Shutdown"
        server.shutdown()
