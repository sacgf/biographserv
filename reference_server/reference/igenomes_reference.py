'''
Created on 03/12/2014

@author: dlawrence
'''
import os

from biographserv import settings

class IGenomesReference(object):
    def __init__(self, organism, annotation_group, build):
        self.organism = organism
        self.annotation_group = annotation_group
        self.build = build
        self.igenomes_base_dir = settings.IGENOMES_DIR
        self.base_dir = os.path.join(self.igenomes_base_dir, organism, annotation_group, build)
        self.annotation_dir = os.path.join(self.base_dir, "Annotation")
        self.genes_dir = os.path.join(self.annotation_dir, "Genes")
        self.genes_gtf = os.path.join(self.genes_dir, "genes.gtf")
        self.ideogram = os.path.join(self.genes_dir, "cytoBand.txt")
        self.sequence_dir = os.path.join(self.annotation_dir, 'Sequence', 'WholeGenomeFasta')
        self.genome_fasta = os.path.join(self.sequence_dir, 'genome.fa')
        self.mature_mirna_fasta = os.path.join(self.annotation_dir, 'SmallRNA', 'mature.fa') 

    def get_reference_id(self):
        return "%s/%s/%s" % (self.organism, self.annotation_group, self.build)
    
    def __str__(self):
        return self.get_reference_id()