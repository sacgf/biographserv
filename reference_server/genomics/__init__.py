"""Genomic Annotations, Objects, and utilities"""

from Bio import SeqFeature, SeqIO
import HTSeq
from collections import defaultdict
import gzip
import os
import random
import re

import numpy as np
from bgs.utils import file_utils
from bgs.utils.utils import int_from_commas


class Reader_format_chrom(object):
    reader_clazz = None
    
    def __init__(self, alignment_filename, want_chr):
        self.reader = self.reader_clazz(alignment_filename)
        self.has_chr = sam_or_bam_has_chrom(self.reader)
        self.want_chr = want_chr
        
        if self.has_chr == self.want_chr: #Use standard reader class if possible
            self.iter_method = self.reader.__iter__
            self.getitem_method = self.reader.__getitem__
        else:
            self.iter_method = self._format_chrom_iter
            self.getitem_method = self._format_chrom_getitem
            
    def _format_chrom_iter(self):
        for aln in self.reader:
            if aln.aligned:
                aln.iv = iv_format_chrom(aln.iv, self.want_chr)
            yield aln
    
    def _format_chrom_getitem(self, iv):
        iv = iv_format_chrom(iv, self.has_chr) # to not get ValueError
        for aln in self.reader[iv]:
            if aln.aligned:
                aln.iv.chrom = format_chrom(aln.iv.chrom, self.want_chr)
            yield aln

    def __iter__(self):
        return self.iter_method()
        
    def __getitem__(self, iv):
        return self.getitem_method(iv)
        
class BAM_Reader_format_chrom(Reader_format_chrom):
    ''' Formats alignments to have/not have 'chr' in chromosome ID
        file name
        Allows random access
        want_chr: Boolean - whether you want "chr" at the beginning of chrom
        return: yields formatted alignment 
    '''
    reader_clazz = HTSeq.BAM_Reader

class SAM_Reader_format_chrom(Reader_format_chrom):
    ''' Formats alignments to have/not have 'chr' in chromosome ID
        file name
        want_chr: Boolean - whether you want "chr" at the beginning of chrom
        return: yields formatted alignment 
    '''
    reader_clazz = HTSeq.SAM_Reader

def sam_or_bam_has_chrom(sam_or_bam):
    ''' return true if sam or bam has chr in header reference names
        warning: this will die if you have BOTH chr and NO chr
    '''
    header_dict = sam_or_bam.get_header_dict()
    
    sq = header_dict['SQ']
    has_chr = 0
    no_chr = 0
    for row in sq:
        chrom = row['SN']
        if chrom.startswith('chr'):
            has_chr += 1
        else:
            no_chr += 1
            
    if has_chr and no_chr:
        raise ValueError("Header has both starting with 'chr' and non-starting with 'chr' reference name!")
    
    return bool(has_chr)

def count_coverage(coverage):
    counts = {}
    for (c, strand) in coverage.chrom_vectors.items():
        tally = 0
        for vec in strand.values():
            for (iv, value) in vec.steps():
                if value > 0:
                    tally += iv.length
                
        counts[c] = tally
    return counts


def coverage_array(bam_file_name, stranded=None):
    if stranded is None:
        stranded = False
    bam_file = HTSeq.BAM_Reader(bam_file_name)
    coverage = HTSeq.GenomicArray( "auto", stranded=stranded, typecode="i" )
    for almnt in bam_file:
        if almnt.aligned:
            coverage[ almnt.iv ] += 1
    
    return coverage

def format_chrom(chrom, want_chr):
    ''' Pass in a chromosome (unknown format), return in your format
        @param chrom: chromosome ID (eg 1 or 'chr1')
        @param want_chr: Boolean - whether you want "chr" at the beginning of chrom
        @return: "chr1" or "1" (for want_chr True/False) 
    '''
    if want_chr:   
        if chrom.startswith("chr"):
            return chrom
        else:
            return "chr%s" % chrom
    else:
        return chrom.replace("chr", "")

def iv_format_chrom(iv, want_chr):
    iv = iv.copy()
    iv.chrom = format_chrom(iv.chrom, want_chr)
    return iv

def GenomicInterval_from_directional( chrom, start_d, length, strand="." ):
    ''' Fix bug in HTSeq:
        HTSeq.GenomicInterval_from_directional throws 'str' object has no attribute 'se' '''
    strand = intern( strand )
    if strand != "-":
        return HTSeq.GenomicInterval( chrom, start_d, start_d+length, strand )
    else:
        return HTSeq.GenomicInterval( chrom, start_d-length+1, start_d+1, strand )

# HTSeq documentation says end = the end of the interval. Following Python convention for ranges, this
# is *ONE MORE* than the coordinate of the last base that is considered part of the sequence.
def last_base(iv):
    last_base_pos = iv.end_as_pos
    last_base_pos.pos -= 1
    return last_base_pos

def mid_point(iv):
    '''Returns genomic position'''
    g_pos = iv.start_as_pos
    g_pos.pos += int(iv.length / 2)
    return g_pos
    
def read_start(iv):
    return iv.start_d_as_pos

def distance(ivA, ivB):
    '''If B is to left of A, distance will be negative'''

    if ivA.chrom != ivB.chrom:
        message = "Both intervals must be on the same contig (A: %s, B: %s)" % (ivA.chrom, ivB.chrom)
        raise ValueError(message)
    
    if ivA.start > ivB.end: # B fully before
        return ivB.end - ivA.start
    elif ivA.end < ivB.start: # B fully after
        return ivB.start - ivA.end
    else:
        return 0
        
def HTSeqFeatureToSeqFeature(htseq_feature):
    iv = htseq_feature.iv
    location = SeqFeature.FeatureLocation(iv.start, iv.end)
    strand = { '-' : -1, '.': 0, '+' : +1 }[iv.strand]
    return SeqFeature.SeqFeature(location, htseq_feature.type, id=iv.chrom, strand=strand)

def HTSeqInterval_to_hash(iv):
    return {'chr' : iv.chrom, 'start' : iv.start, 'stop' : iv.end, 'strand' : iv.strand}

def transcript_iv(transcript_id, start_mpos, end_mpos):
    return HTSeq.GenomicInterval(transcript_id, start_mpos, end_mpos + 1, '.')

def iv_from_pos_range(g_pos, range_length):
    '''p = Genomic position. 
    Returns iv 'range_length' bp upstream and 'range_length' downstream of position p'''
    return HTSeq.GenomicInterval( g_pos.chrom, g_pos.pos - range_length, g_pos.pos + range_length, g_pos.strand)

def iv_from_pos_length(g_pos, length):
    ''' returns an iv from g_pos ... +length in STRANDED direction '''
    return GenomicInterval_from_directional( g_pos.chrom, g_pos.pos, length, g_pos.strand )

def iv_from_pos_directional_before_after(g_pos, upstream_length, downstream_length):
    '''Note: The g_pos base is assumed to be included in downstream_length
    e.g upstream_length=100, downstream_length=100 has total length=200 bp
    Check that this function does what you're expecting - setting upstream length to 0 will change the position of start_d on neg strand...'''
    if g_pos.strand == '+':
        start = g_pos.pos - upstream_length
        end = g_pos.pos + downstream_length
    elif g_pos.strand == '-':
        start = g_pos.pos - downstream_length
        end = g_pos.pos + upstream_length
    else:
        raise ValueError("unknown strand in genomic position %s" % g_pos)

    return HTSeq.GenomicInterval( g_pos.chrom, start, end, g_pos.strand)

def iv_from_pos_and_offsets(g_pos, offset1, offset2):
    '''offset1 and offset2 are stranded distance from g_pos
    + = offset in 3' direction'''
    if g_pos.strand == '+':
        start = g_pos.pos + min([offset1, offset2])
        end = g_pos.pos + max([offset1, offset2])
    if g_pos.strand == '-':
        start = g_pos.pos - max([offset1, offset2]) + 1
        end = g_pos.pos - min([offset1, offset2]) + 1
    return HTSeq.GenomicInterval(g_pos.chrom, start, end, g_pos.strand)    

def iv_directional_extend(iv, upstream_length, downstream_length):
    if iv.strand == '+':
        start = iv.start - upstream_length
        end = iv.end + downstream_length
    elif iv.strand == '-':
        start = iv.start - downstream_length
        end = iv.end + upstream_length
    else:
        raise ValueError("unknown strand in genomic interval %s" % iv)

    return HTSeq.GenomicInterval(iv.chrom, start, end, iv.strand)    
    

    
def gff_to_hash(gff, index_attribute):
    indexed_gff = {}
    for feature in HTSeq.GFF_Reader(gff):
        index = feature.attr[index_attribute]
        if index in indexed_gff:
            message = "%s non-unique index!" % index_attribute
            raise ValueError(message)
        indexed_gff[index] = feature
    return indexed_gff

def gff_to_hash_of_lists(gff, index_attribute, feature_type=None):    
    indexed_gff_features = defaultdict(list)
    for feature in HTSeq.GFF_Reader(gff):
        if feature_type is None or feature.type == feature_type:
            indexed_gff_features[feature.attr[index_attribute]].append(feature)
    return indexed_gff_features

def fasta_to_hash(fasta):
    indexed_fasta = {}
    for record in SeqIO.parse(fasta, "fasta"):
        indexed_fasta[record.id] = str(record.seq)
    return indexed_fasta

def features_to_genomic_array(features, stranded):
    ga = HTSeq.GenomicArray( "auto", stranded=stranded, typecode='O' )
    for feature in features:
        ga[feature.iv] = feature
    return ga

def features_to_genomic_array_of_sets(feature_iterator, **kwargs):
    '''By default, the features must be stranded. Can set this via kwargs 'stranded' as False.'''
    is_stranded = kwargs.get("stranded", True)
    
    genomic_array_of_features = HTSeq.GenomicArrayOfSets("auto", stranded=is_stranded)
    
    for feature in feature_iterator:
        genomic_array_of_features[feature.iv] += feature
    return genomic_array_of_features

def get_unique_features_from_genomic_array_of_sets_iv(genomic_array_of_sets, iv):
    '''Collapse genomic array of sets into a unique list of the values in that region'''
    all_values = set()
    for iv, values_in_iv in genomic_array_of_sets[iv].steps():
        all_values.update(values_in_iv)
    return all_values

def get_max_iv(genomic_array, iv, getter_func=None):
    '''
    Get the highest value in iv
    getter_func = Function to extract the score from genomic array value(s)
    Default = genomic array value
    '''
    return get_iv_value(genomic_array, iv, max, getter_func)

def get_iv_value(genomic_array, iv, operation, getter_func=None):
    if getter_func is None: 
        getter_func = lambda x : x
        
    values = []
    for (_, raw_value) in genomic_array[iv].steps():
        if raw_value:
            value = getter_func(raw_value)
            values.append(value)

    if values:
        return operation(values)
    else:
        return 0

def iv_format(iv):
    return "%s:%d-%d" % (iv.chrom, iv.start, iv.end)

def iv_format_with_strand(iv):
    return "%s:%d-%d_%s" % (iv.chrom, iv.start, iv.end, iv.strand)

def iv_from_string(locus_string):
    ''' locus_string in format: "chr1:228,336,739-228,353,206" '''
    regex = "(.*):(.*)-(.*)"
    m = re.match(regex, locus_string)
    if not m:
        msg = "'%s' did not match expected regex '%s' for locus" % (locus_string, regex)
        raise ValueError(msg)
    (chrom, start, end) = m.groups()
    start = int_from_commas(start)
    end = int_from_commas(end)
    return HTSeq.GenomicInterval(chrom, start, end, '.')


def create_coverage_array(iterator):
    coverage = HTSeq.GenomicArray( "auto", stranded=True, typecode='i' )
    for iv in iterator:
        coverage[iv] += 1
    return coverage

def get_open_func(file_name):
    if file_name.endswith(".gz"):
        return gzip.open
    else:
        return open

def open_handle_gz(file_name, *args):
    of = get_open_func(file_name)
    return of(file_name, *args)

def get_with_gzip_extension(file_name):
    (name, extension) = os.path.splitext(file_name)
    if extension == ".gz":
        extension = os.path.splitext(name)[1] + extension
    return extension

def get_non_gzip_extension(file_name):
    (name, extension) = os.path.splitext(file_name)
    if extension == ".gz":
        extension = os.path.splitext(name)[1]
    return extension


def get_non_gzip_name(filename):
    filename = file_utils.remove_gz_if_exists(filename)
    return os.path.splitext(os.path.basename(filename))[0]

def sequence_file_format(filename):
    filename = file_utils.remove_gz_if_exists(filename)
        
    if [f for f in [".fa", ".fasta"] if filename.endswith(f)]:
        return "fasta"
    elif [f for f in [".fq", ".fastq", ".txt"] if filename.endswith(f)]:
        return "fastq"
    raise Exception("Unknown sequence file type of %s" % (filename, ))

def other_bases(base):
    bases = set("GATC")
    bases.remove(base)
    return list(bases)

def genomic_alternative_matches(base):
    return "[%s]" % "".join(other_bases(base))

def random_index(array):
    return int(random.random()*len(array))

def different_base(base):
    bases = other_bases(base)
    return bases[random_index(bases)]

def mutate_sequence(sequence):
    i = random_index(sequence)
    return sequence[:i] + different_base(sequence[i]) + sequence[i+1:]

def generate_non_exact_mis_matches(sequence, num_mis_matches):
    assert num_mis_matches > 0
    return generate_non_exact_mis_matches_list(list(sequence), num_mis_matches)

def generate_non_exact_mis_matches_list(sequence_list, num_mis_matches):
    mismatches = set()

    if num_mis_matches > 0:
        for (i, orig) in enumerate(sequence_list):
            mm_list = list(sequence_list)
            if len(orig) == 1:
                mm_list[i] = genomic_alternative_matches(orig)
            mismatches.update(generate_non_exact_mis_matches_list(mm_list, num_mis_matches-1))
    else:
        mismatches.add(''.join(sequence_list))
    
    return mismatches

def directional_offset(g_pos, offset):
    '''Returns offset * +1 for + strand, *-1 for - strand'''
    if g_pos.strand == '-':
        return -offset
    elif g_pos.strand == '+':
        return offset
    else:
        raise ValueError("unknown strand in genomic position %s" % g_pos)

def stranded_offset(g_pos, offset):
    ''' Returns Genomic Position moved by offset bp in stranded direction '''
    return HTSeq.GenomicPosition(g_pos.chrom, g_pos.pos + directional_offset(g_pos, offset), g_pos.strand)

def sorted_genomic_positions(gpos_a, gpos_b):
    if gpos_a.pos < gpos_b.pos:
        return gpos_a, gpos_b
    else:
        return gpos_b, gpos_a

def stranded_func(a, b, pos_func, neg_func, key=None):
    assert a.strand == b.strand
    strand = a.strand
    if strand == '+':
        func = pos_func
    elif strand == '-':
        func = neg_func
    else:
        raise ValueError("stranded test requires strand (%s) be '-' or '+'" % strand)

    return func(a, b, key=key)

def genomic_position_key(g_pos):
    return g_pos.pos
    
def stranded_min_pos(a, b):
    return stranded_func(a, b, min, max, genomic_position_key)
        
def stranded_max_pos(a, b):
    return stranded_func(a, b, max, min, genomic_position_key)

def gc_count(seq):
    from collections import Counter #this is a quick-n-dirty fix so that we can load this script on the server without immediate errors
    counter = Counter(seq.upper())
    return counter['G'] + counter['C']

def gc_percent(seq):
    return 100.0 * gc_count(seq) / len(seq)


class GenomicArrayofFeatures(object):
    def __init__(self, feature_iterator):
        self.genomic_array_of_features = features_to_genomic_array_of_sets(feature_iterator)
            
    def features_from_iv(self, iv):
        for (_, value) in self.genomic_array_of_features[iv].steps():
            for feature in value:
                yield feature

    def unique_features_from_iv(self, iv):
        seen = set()
        for feature in self.features_from_iv(iv):
            if not feature in seen:
                seen.add(feature)
                yield feature

def stranded_offset_between(first_gpos, second_gpos):
    ''''''
    
    assert first_gpos.strand == second_gpos.strand, "%s and %s on same strand" % (first_gpos, second_gpos)
    strand = first_gpos.strand
    if strand == '+':
        return second_gpos.pos - first_gpos.pos
    else:
        return first_gpos.pos - second_gpos.pos

def get_subsequences_set(sequence, sub_sequence_length):
    subsequence_set = set()

    for j in range(len(sequence) - sub_sequence_length + 1):
        subsequence = sequence[j:j+sub_sequence_length]
        subsequence_set.add(subsequence)
    return subsequence_set

def iv_opposite_strand(iv):
    iv = iv.copy()
    iv.strand = opposite_strand(iv.strand)
    return iv

def iv_unstranded(iv):
    iv = iv.copy()
    iv.strand = '.'
    return iv

def opposite_strand(strand):
    if strand == '-':
        return '+'
    elif strand == '+':
        return '-'
    else:
        raise ValueError("unknown strand '%s'" % strand)
    
def sort_transcripts_by_length(transcripts):
    key = lambda t : t.iv.length
    return sorted(transcripts, key=key)
    
