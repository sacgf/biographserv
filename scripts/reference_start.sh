#!/bin/bash

export PROJECT_DIR="$(dirname $0)/.."
export PYTHONPATH=${PROJECT_DIR}
python ${PROJECT_DIR}/reference_server/server_main.py --build=hg19
