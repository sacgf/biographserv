BioGraphServ is a webapp which generates graphs from small bioinformatics file formats (BED, Gene lists, VCF etc)

It is free to use, with no login required: http://biographserv.com

If you use BioGraphServ in a paper, please cite the webpage for now - thanks. (I will start writing a paper in the 2nd half of 2015)

## LICENCE

The code is released under the [Creative Commons by Attribution](https://creativecommons.org/licenses/by/3.0/au/deed.en) licence. You are free to use and modify it for any purpose (including commercial), so long as you cite/link http://biographserv.com (or a paper if one exists).

## CONTRIBUTING

I would love help - either implementing new features or improving existing ones.

To get started, see the [Issue Tracker](https://bitbucket.org/sacgf/biographserv/issues?status=new&status=open), and [install a local copy](https://bitbucket.org/sacgf/biographserv/wiki/InstallBioGraphServ/)
