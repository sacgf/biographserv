'''
Created on 23/12/2014

@author: dlawrence
'''

from django.core.urlresolvers import reverse
from django.utils.functional import SimpleLazyObject

from bgs.jqgrid import DataFrameJqGrid
from bgs.models import auto_combo_dataframe

class ComboGrid(DataFrameJqGrid):
    def __init__(self, combo):
        kwargs={"project_id" : combo.project.pk, "combo_id" : combo.pk}
        self.url = SimpleLazyObject(lambda : reverse("combo_grid_handler", kwargs=kwargs))
        super(ComboGrid, self).__init__()
        self.combo = combo

    def get_dataframe(self):
        return self.combo.get_dataframe()

class AutoComboGrid(DataFrameJqGrid):
    def __init__(self, project_id, result_type):
        kwargs={"project_id" : project_id, "result_type" : result_type}
        self.url = SimpleLazyObject(lambda : reverse("auto_combo_grid_handler", kwargs=kwargs))
        super(AutoComboGrid, self).__init__()
        self.project_id = project_id
        self.result_type = result_type

    def get_dataframe(self):
        return auto_combo_dataframe(self.project_id, self.result_type)

