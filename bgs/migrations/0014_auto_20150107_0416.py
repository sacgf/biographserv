# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0013_fileprocessingtask_combo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fileprocessingtask',
            name='uploaded_file',
            field=models.ForeignKey(to='bgs.UploadedFile', null=True),
            preserve_default=True,
        ),
    ]
