# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0016_remove_graphgroup_sort_order'),
    ]

    operations = [
        migrations.AddField(
            model_name='graphsamplegrouprelationship',
            name='graph_category',
            field=models.ForeignKey(default=1, to='bgs.GraphCategory'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='graphsamplegrouprelationship',
            name='graph_group',
            field=models.ForeignKey(to='bgs.GraphGroup', null=True),
            preserve_default=True,
        ),
    ]
