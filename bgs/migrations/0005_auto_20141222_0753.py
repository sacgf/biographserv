# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0004_auto_20141218_0634'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='combocolumn',
            name='csv_column',
        ),
        migrations.AddField(
            model_name='combocolumn',
            name='combo',
            field=models.ForeignKey(default=-1, to='bgs.Combo'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='combocolumn',
            name='sort_order',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='csvresult',
            name='result_type',
            field=models.CharField(max_length=1, choices=[(b'R', b'Region Stats'), (b'G', b'Gene Scores'), (b'O', b'Other'), (b'S', b'Uploaded Single Sample Expression'), (b'M', b'Uploaded Multi Sample Expressions'), (b'C', b'Uploaded CuffDiff')]),
            preserve_default=True,
        ),
    ]
