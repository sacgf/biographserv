# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0006_auto_20141223_0459'),
    ]

    operations = [
        migrations.CreateModel(
            name='GenomicRange',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
                ('anchor', models.CharField(max_length=1, choices=[(b'P', b'TSS'), (b'G', b'Gene Start/End')])),
                ('upstream', models.IntegerField()),
                ('downstream', models.IntegerField()),
                ('max_size', models.IntegerField()),
                ('step_size', models.IntegerField()),
                ('project', models.ForeignKey(to='bgs.Project')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='csvresult',
            name='result_type',
            field=models.CharField(max_length=1, choices=[(b'R', b'Region Stats'), (b'G', b'Bed Gene Scores'), (b'O', b'Other'), (b'S', b'Uploaded Single Sample Expression'), (b'M', b'Uploaded Multi Sample Expressions'), (b'C', b'Uploaded CuffDiff')]),
            preserve_default=True,
        ),
    ]
