# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0007_auto_20141229_1257'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='read_only',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
