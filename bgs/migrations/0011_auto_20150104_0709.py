# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0010_auto_20150104_0231'),
    ]

    operations = [
        migrations.AddField(
            model_name='styledgraph',
            name='has_colormap',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='styledgraph',
            name='number_of_colors',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
