'''
Created on 08/12/2016

@author: dlawrence
'''

import numpy as np

from bgs.models_enum import CSVResultType, GraphType

VOLCANO_X_MIN = -20
VOLCANO_X_MAX = 20

# These functions return data = [], layout = {} which are converted to JSON and passed to Plotly

def series_to_bar(series):
    return [{"x" : series.index.tolist(),
             "y" : series.values.tolist(),
             "type" : 'bar',}]

def df_to_heatmap(df):
    return [{"z" : df.as_matrix().tolist(),
             "x" : df.columns.tolist(),
             "y" : df.index.tolist(),
             "type" : 'heatmap',}]


def df_to_text_scatter(df):
    x = df.ix[0].values.tolist()
    y = df.ix[1].values.tolist()
    labels = df.columns.tolist()
    
    return [{"x" : x,
             "y" : y,
             "mode" : "text",
             "type" : "scatter",
             "text" : labels}]

def series_to_line(series):
    return [{"x" : series.index.tolist(),
             "y" : series.values.tolist(),
             "mode" : 'lines',}]

def cuffdiff_df_to_volcano(df):
    FOLD_CHANGE = 'log2(fold_change)'
    P_VALUE = "p_value"

    P_MAX_CUT_OFF = 0.5
    FOLD_CHANGE_MIN_CUT_OFF = 1.2

    p_cut_off_mask = df[P_VALUE] < P_MAX_CUT_OFF
    fold_change_cut_off_mask = df[FOLD_CHANGE].abs() > FOLD_CHANGE_MIN_CUT_OFF

    print "Orig: %d" % len(df)
    df = df[p_cut_off_mask & fold_change_cut_off_mask]
    print "Filtered: %d" % len(df)
    
    x = df[FOLD_CHANGE]
    y = -np.log10(df[P_VALUE])

    labels = df.index.tolist()

    scatter_plot = {"x" : x.values.tolist(),
                    "y" : y.values.tolist(),
                    "mode" : "markers",
                    "type" : "scatter",
                    "marker" : {'color' : 'rgba(255, 0, 0, .5)'},
                    "text" : labels}

    significant = -np.log10(0.05)
    h_line = {"x" : [VOLCANO_X_MIN, VOLCANO_X_MAX],
              "y" : [significant, significant],
              "mode" : 'lines',
              "name" : 'p = 0.05',
              "line" : {"dash" : 'dot',
                        "color" : "purple"}}
        
    return [scatter_plot, h_line]
    

DATA_SOURCE_CONVERTERS = {
    CSVResultType.CHROMOSOME_BIN_SCORES : {},
    CSVResultType.CORRELATION_MATRIX : {GraphType.CORRELATION_MATRIX : df_to_heatmap},
    CSVResultType.CUFFDIFF : {GraphType.VOLCANO : cuffdiff_df_to_volcano},
    CSVResultType.GENE_SCORES : {},
    CSVResultType.MULTI_SAMPLE_EXPRESSION : {},
    CSVResultType.OTHER : {},
    CSVResultType.READ_COUNTS : {GraphType.BAR : series_to_bar},
    CSVResultType.REGION_STATS : {},
    CSVResultType.SINGLE_SAMPLE_EXPRESSION : {},
    CSVResultType.SVD_SCREE : {GraphType.LINE : series_to_line},
    CSVResultType.SVD_VT : {GraphType.TEXT_SCATTER : df_to_text_scatter},
    CSVResultType.VCF_INFO_COUNTS : {},
}

def get_csv_data_source(csv_file_name, result_type, graph_type):
    print "get_csv_data_source(%s, %s, %s)" % (csv_file_name, result_type, graph_type)
    loader = CSVResultType.LOADERS[result_type]
    raw_data = loader(csv_file_name)
    graph_converters = DATA_SOURCE_CONVERTERS[result_type] 
    converter = graph_converters[graph_type]
    return converter(raw_data)
