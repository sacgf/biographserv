'''
Created on 08/12/2016

@author: dlawrence
'''
from bgs.models_enum import GraphType
from bgs.plotly_json.plotly_data import VOLCANO_X_MIN, VOLCANO_X_MAX


LAYOUTS = {
    GraphType.VOLCANO : {"xaxis" : {"title" : "log2 fold change",
                                    "range" : [VOLCANO_X_MIN, VOLCANO_X_MAX],
                                    "autorange" : False,
                                    'zeroline': False,
                                    "showline" : False},
                         "yaxis" : {"title" : "-log10(p-value)",
                                    'rangemode': 'nonnegative',
                                    'zeroline': False,
                                    #"autorange" : False,
                                    "showline" : False}}
}




def get_graph_layout(graph):
    layout = LAYOUTS.get(graph.graph_type, {})
    return layout
