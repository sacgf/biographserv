'''
Created on 18/12/2014

@author: dlawrence
'''

import pandas as pd

def delimited_file_has_header(f):
    ''' returns (has_header, num_columns, reason) '''

    df = pd.DataFrame.from_csv(f, index_col=False, header=None, sep=None)
    row = df.ix[0]
    num_columns = len(row)

    if num_columns < 1:
        msg = "Delimited file only has %d columns, require >= 2" % num_columns
        raise ValueError(msg)

    columns_numeric = True
    reason = None
    for i in range(1, len(row)):
        col = row[i]
        try:
            float(col)
        except ValueError:
            columns_numeric = False
            reason = 'Column %d has non-numeric value "%s"' % (i, col)
            break
        
    has_header = not columns_numeric
    return (has_header, num_columns, reason)

def get_subclass_or_create(super_clazz, clazz, *args, **kwargs):
    try:
        obj = super_clazz.objects.get_subclass(*args, **kwargs)
        created = False
    except super_clazz.DoesNotExist:
        obj = clazz(*args, **kwargs)
        obj.save()
        created = True
    return obj, created