import codecs
from django.template.loader import render_to_string
import importlib
import locale
import logging
import os
import re
import sys
import traceback

from bgs.utils.file_utils import mk_path_for_file


def render_to_file(filename, template, context):
    mk_path_for_file(filename)
    codecs.open(filename, 'w', 'utf-8').write(render_to_string(template, context))

"""
@see http://code.activestate.com/recipes/363602
"""
class Lazy(object):
    def __init__(self, calculate_function):
        self._calculate = calculate_function

    def __get__(self, obj, _=None):
        if obj is None:
            return self
        value = self._calculate(obj)
        setattr(obj, self._calculate.func_name, value)
        return value

def sorted_nicely(l): 
    """ Sort the given iterable in the way that humans expect.
        From http://www.codinghorror.com/blog/2007/12/sorting-for-humans-natural-sort-order.html """ 
    convert = lambda text: int(text) if text.isdigit() else text 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

def full_class_name(klass):
    return klass.__module__ + '.' + klass.__name__

def import_class(full_class_name):
    (modulename, classname) = full_class_name.rsplit('.', 1)
    print "modulename = %s" % modulename
    mod = importlib.import_module(modulename)
    return getattr(mod, classname)

def int_with_commas(n):
    ''' returns a string formatted with thousand separators '''
    # Requires python > 2.7
    # return "{:,}".format(value)
    locale.setlocale(locale.LC_ALL, '')
    return locale.format("%d", n, grouping=True)

def int_from_commas(string_with_commas):
    ''' returns a string formatted with thousand separators '''
    # Requires python > 2.7
    # return "{:,}".format(value)
    locale.setlocale( locale.LC_ALL, 'en_US.UTF-8' ) 
    return locale.atoi(string_with_commas)

class Struct:
    def __init__(self, **entries): self.__dict__.update(entries)

def setup_console_logger(**kwargs):
    ''' Return a logger defaulting to stderr, INFO, BASIC_FORMAT  '''

    stream = kwargs.get("stream", sys.stderr)
    log_level = kwargs.get("log_level", logging.INFO)
    log_format = kwargs.get("log_format", logging.BASIC_FORMAT)
    name = kwargs.get("name")
    
    
    logger = logging.getLogger(name)
    logger.setLevel(log_level)
    handler = logging.StreamHandler(stream)
    handler.setFormatter(logging.Formatter(log_format))
    logger.addHandler(handler)
    return logger 

def up_dir(path, n):
    ''' Basically like os.path.join(path, "../" * n) but without the dots '''
    assert n >= 0
    path_components = path.split(os.path.sep)
    return os.path.sep.join(path_components[:-n])


def log_traceback():
    exec_type, exec_value, exc_traceback = sys.exc_info()
    logging.error("%s: %s" % (exec_type, exec_value))
    tb = ''.join(traceback.format_tb(exc_traceback))
    logging.error(tb)
