'''
Created on 01/05/2014

@author: dlawrence
'''
from genericpath import commonprefix

def common_suffix(list_of_strings):
    reversed_strings = [list(s[::-1]) for s in list_of_strings]
    return ''.join(reversed(''.join(commonprefix(reversed_strings))))
    
def remove_common_suffix(strings):
    suffix = common_suffix(strings)
    print "suffix = %s" % suffix
    out_strings = []
    if suffix:
        for s in strings:
            i = s.rfind(suffix)
            out_strings.append(s[:i])
    else:
        out_strings = strings

    return out_strings