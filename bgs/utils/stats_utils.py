'''
Helpers for SciPy and numpy

Created on Jan 2, 2013

@author: dave
'''

from scipy import stats
import numpy as np
import pandas as pd
from bgs.utils.string_utils import common_suffix


PEARSON = "PEARSON"
SPEARMAN = "SPEARMAN"

def cap_series_values(series, capped_value):
    '''Cap values in a pd.Series that are more extreme than +/- capped_value. Works in place'''
    series[series > capped_value] = capped_value
    series[series < -capped_value] = -capped_value    

def cap_column_values(dataframe, column_name, capped_value):
    ''' Cap values in a dataframe column that are more extreme than +/- capped_value. Works in place'''
    column = dataframe[column_name]
    column[ column > capped_value ] = capped_value
    column[ column < -capped_value ] = -capped_value

def cap_value(value, capped_value):
    if value > capped_value:
        return capped_value
    elif value < -capped_value:
        return -capped_value
    else:
        return value

def dataframe_counts_to_rpm(df, exclude_columns=None):
    ''' Convert a dataframe of counts to reads per million
        Exclude columns are skipped then added back on
    '''
    columns = df.columns
    if exclude_columns is None:
        exclude_columns = []

    include_columns = list(set(df.columns) - set(exclude_columns))
    include_df = df[include_columns]
    rpm_df = include_df.divide(include_df.sum() / 1e6)
    exclude_df = df[exclude_columns]
    return exclude_df.merge(rpm_df, left_index=True, right_index=True)[columns]

def rename_common_suffix_columns(df, columns):
    return rename_common_suffix(df, "columns", columns)

def rename_common_suffix_index(df, index):
    return rename_common_suffix(df, "index", index)


def rename_common_suffix(df, axis, labels):
    ''' axis = "index" or "columns" '''
    suffix = common_suffix(labels)
    if suffix:
        renamed_labels = []
        for c in labels:
            renamed_labels.append(c[:c.rfind(suffix)])
        rename_dict = dict(zip(labels, renamed_labels))
        return df.rename(**{axis : rename_dict})
    else:
        return df

def dataframe_dropna(df, columns):
    return df[columns].dropna(how='any')

def dataframe_columns_nona(df, col_a, col_b):
    columns = list(set([col_a, col_b])) # In case they are same, so we don't get groupby problem
    two_cols_df = dataframe_dropna(df, columns)
    return (two_cols_df[col_a], two_cols_df[col_b]) 
    
def get_correlation(x, y, **kwargs):
    correlation = kwargs.pop('correlation')
    if correlation == SPEARMAN:
        correlation_coefficient, _ = stats.spearmanr(x, y)
    elif correlation == PEARSON:
        r_value = stats.linregress(x, y)[2]
        correlation_coefficient = r_value ** 2
    else:
        raise ValueError("Unknown correlation type '%s'" % correlation)
    return correlation_coefficient

def correlation_dataframe(df, **kwargs):
    '''Returns: Dataframe of Pearson correlation r^2 values
    kwargs:
        correlation: SPEARMAN / PEARSON
    '''
    
    correlation = kwargs.get("correlation", PEARSON)
    names = df.columns
    mat = pd.DataFrame(index=names, columns=names, dtype='d')

    # Since this is symmetrical matrix, we can just calculate once
    # and enter twice to save a bit of time.
    
    for row in names:
        for column in names:
            if np.isnan(mat[column][row]):
                x, y = dataframe_columns_nona(df, column, row)
                r_squared = get_correlation(x, y, correlation=correlation)
                mat[column][row] = r_squared 
                mat[row][column] = r_squared

    return mat
