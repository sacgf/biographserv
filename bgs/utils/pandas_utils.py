'''
Created on 05/12/2016

@author: dlawrence
'''

import pandas as pd
from reference_server.genomics import open_handle_gz


def load_single_column_df(file_name, name=None):
    kwargs = {'header' : None, 'sep' : None}
    open_file = open_handle_gz(file_name)
    
    if name is None:
        df = pd.DataFrame.from_csv(open_file, **kwargs)
    else:
        df = pd.read_table(open_file, names=[name], **kwargs)
    return df

def load_multi_column_df(file_name, name=None):
    ''' Name not used '''
    open_file = open_handle_gz(file_name)
    return pd.DataFrame.from_csv(open_file, sep=None)

def load_series(file_name, name=None):
    return pd.Series.from_csv(file_name)