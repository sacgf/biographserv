from django.test import TestCase
import os

from bgs.parsers.bed_file import BedFileReader


# Create your tests here.
class TestBedReader(TestCase):
    ENCODE_PEAK_FEATURES = ['signalValue', 'pValue', 'qValue', 'peak']
    TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "test_data", "bed")

    def data(self, file_name):
        return os.path.join(self.TEST_DATA_DIR, file_name)

    def test_bed(self):
        reader = BedFileReader(self.data("test.bed"))
        feature = reader.next()
        self.assertTrue(feature.iv.length > 0)
        self.assertIsNotNone(feature.score, "bed has score")


    def test_encode_narrow_peak(self):
        reader = BedFileReader(self.data("test.narrowPeak"), bed_type='from_filename')
        feature = reader.next()
        for f in self.ENCODE_PEAK_FEATURES:
            msg = "narrow peak has non none '%s'" % f
            self.assertIsNotNone(feature.attr[f], msg)
        
    def test_encode_broad_peak(self):
        reader = BedFileReader(self.data("test.broadPeak"), bed_type='from_filename')
        feature = reader.next()
        for f in self.ENCODE_PEAK_FEATURES[:3]:
            msg = "broad peak has non none '%s'" % f
            self.assertIsNotNone(feature.attr[f], msg)

    def test_bed_format_chrom(self):
        reader = BedFileReader(self.data("test_no_chrom.bed"))

        any_have_chrom = False
        for feature in reader:
            any_have_chrom |= feature.iv.chrom.startswith("chr")
        self.assertFalse(any_have_chrom, "No features start with 'chr'")

        reader = BedFileReader(self.data("test_no_chrom.bed"), want_chr=True)

        all_have_chrom = True
        for feature in reader:
            all_have_chrom &= feature.iv.chrom.startswith("chr")

        self.assertTrue(all_have_chrom, "all features start with 'chr'")


