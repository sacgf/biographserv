from collections import OrderedDict
from datetime import datetime
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.forms.models import inlineformset_factory
from django.forms.widgets import HiddenInput, TextInput
from django.http.response import HttpResponse, Http404
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template.context import RequestContext
from django.template.loader import render_to_string
from django.views.decorators.http import require_POST
from jfu.http import upload_receive, UploadResponse, JFUResponse
import json
import os
import shutil
import tempfile
import uuid
import numpy as np

from bgs import models, forms
from bgs.forms import get_styled_graph_form_class
from bgs.grids import ComboGrid, AutoComboGrid
from bgs.models import UploadedFileTypes, UploadedFile, ProcessingStatus, ComboColumn, \
    get_processing_dir, StyledGraph, CSVResult, auto_combo_dataframe, CSVResultType
from bgs.processing import process_uploaded_file, reference_changed, \
    get_file_types_csv_result_types_lookup, \
    get_uploaded_files_producing_result_type, genomic_regions_changed, \
    process_combo
from bgs.utils.decorators import json_view
from biographserv import settings
import pandas as pd


def get_subclass_or_404(klass, **kwargs):
    try:
        return klass.objects.get_subclass(**kwargs)
    except klass.DoesNotExist: #@UndefinedVariable
        raise Http404('No subclass of %s (%s)' % (klass, kwargs))

def index(request):
    project_id = uuid.uuid4().hex
    return redirect('view_project', project_id)

PROJECT_FIELDS = ['organism', 'build', 'name', 'description', 'email']

def view_project(request, project_id):
    file_type_csv_result_type_lookup_json = json.dumps(get_file_types_csv_result_types_lookup())

    context = {'project_id' : project_id,
               'csv_result_type_name_lookup' : json.dumps(dict(CSVResultType.CHOICES)),
               'file_type_csv_result_type_lookup' : file_type_csv_result_type_lookup_json,
               'version' : settings.VERSION}

    try:
        project = models.Project.objects.get(pk=project_id)
        file_dicts = []
        for uploaded_file in project.uploadedfile_set.all():
            file_dicts.append(uploadedfile_dict(uploaded_file))

        combo_dicts = project.get_combos()
        
        context['project'] = project
        context['existing_files'] = json.dumps(file_dicts)
        context['existing_combos'] = json.dumps(combo_dicts)
    except models.Project.DoesNotExist: # Maybe not saved yet
        pass
    
    return render_to_response('bgs/index.html', RequestContext(request, context))

def edit_project(request, project_id, initial_message=None, center_panel_save_redirect_url=None):
    PROJECT_FIELDS = ['organism', 'build', 'name', 'description', 'email']
    project = get_object_or_404(models.Project, pk=project_id)
    project_read_only_check(request, project)
    widgets = {f : HiddenInput() for f in ['name', 'anchor', 'max_size', 'step_size']}
    genomic_range_fields = ['project', 'name', 'anchor', 'upstream', 'downstream', 'max_size', 'step_size']
    GenomicRangeFormSet = inlineformset_factory(models.Project, models.GenomicRange, widgets=widgets, extra=0, can_delete=False, fields=genomic_range_fields)

    if request.POST:
        old_build = project.build

        project_form = forms.ProjectForm(request.POST)
        genomic_ranges_formset = GenomicRangeFormSet(request.POST, instance=project)

        if project_form.is_valid() and genomic_ranges_formset.is_valid():
            for field in PROJECT_FIELDS:
                setattr(project, field, project_form.cleaned_data[field])
            project.save()
            genomic_ranges_formset.save()
            
            if old_build != project.build:
                reference_changed(project)

            if genomic_ranges_formset.has_changed():
                genomic_regions_changed(project)
    else:
        initial = {f : getattr(project, f) for f in PROJECT_FIELDS}
        project_form = forms.ProjectForm(initial=initial)
        genomic_ranges_formset = GenomicRangeFormSet(instance=project)

    context = {'initial_message' : initial_message,
               'center_panel_save_redirect_url' : center_panel_save_redirect_url,
               'project' : project,
               'project_form' : project_form,
               'genomic_ranges_formset' : genomic_ranges_formset,
               'genomic_anchor_lookup' : json.dumps(dict(models.GenomicAnchor.GENOMIC_ANCHOR_CHOICE))}
    return render_to_response('bgs/edit_project.html', RequestContext(request, context))

@require_POST
def delete_project(request, project_id):
    project = models.Project.objects.get(pk=project_id)
    project_read_only_check(request, project)

    # Collect some stats
    num_deleted_uploaded_files = project.uploadedfile_set.count()
    num_deleted_graphs = StyledGraph.get_for_project(project_id).count()
    num_deleted_results = CSVResult.get_for_project(project_id).count()
    project.delete()

    project_dir = get_processing_dir(project_id)
    if os.path.exists(project_dir):
        shutil.rmtree(project_dir)

    context = {'project_id' : project_id,
               'deleted_uploaded_files' : num_deleted_uploaded_files,
               'deleted_graphs' : num_deleted_graphs,
               'deleted_results' : num_deleted_results,
    }
    return render_to_response('bgs/deleted_project.html', RequestContext(request, context))


def download_project(request, project_id):
    project = get_object_or_404(models.Project, pk=project_id)
    processing_dir = models.get_processing_dir(project.pk)
    tmpdir = tempfile.mkdtemp()
    try:
        tmparchive = os.path.join(tmpdir, 'archive')
        temp_zip = shutil.make_archive(tmparchive, 'zip', processing_dir)
        response = HttpResponse(open(temp_zip), content_type='application/zip')
        response['Content-Disposition'] = 'attachment; filename=biographserv_%s.zip' % project_id
        return response    
    finally:
        shutil.rmtree(tmpdir)


def uploadedfile_dict(uploaded_file):
    return {
        'uploaded_file_id' : uploaded_file.pk,
        'name' : uploaded_file.name,
        'size' : uploaded_file.uploaded_file.size,
        'file_type' : uploaded_file.file_type,
        'url': 'javascript:viewUploadedFile(%d);' % uploaded_file.pk,
        'deleteUrl': reverse('jfu_delete', kwargs = { 'pk': uploaded_file.pk }),
        'deleteType': 'POST',
    }
    

@require_POST
def upload(request):
    # The assumption here is that jQuery File Upload
    # has been configured to send files one at a time.
    # If multiple files can be uploaded simulatenously,
    # 'file' may be a list of files.
    print "views.upload() START"

    try:
        uploaded_file = upload_receive( request )
        project_id = request.POST["project_id"]
        
        (project, created) = models.Project.objects.get_or_create(pk=project_id)
        if created:
            project.create_default_genomic_ranges()
        else:
            project_read_only_check(request, project)
    
        file_type = UploadedFileTypes.get_file_type_from_extension(uploaded_file)
        instance = UploadedFile(project=project,
                                name=uploaded_file._name,
                                file_type=file_type,
                                uploaded_file=uploaded_file,
                                date=datetime.now())
        instance.save()
        if not instance.needs_reference_set():
            process_uploaded_file(instance)
    
        file_dict = uploadedfile_dict(instance)
    except Exception as e:
        print e
        file_dict = {"error" : str(e)}

    print "views.upload() END"
    return UploadResponse( request, file_dict )

@require_POST
def upload_delete( request, pk ):
    try:
        instance = UploadedFile.objects.get(pk=pk)
        project_read_only_check(request, instance.project)

        data = {'uploaded_file_id' : instance.pk,
                'file_type' : instance.file_type}
        os.unlink( instance.uploaded_file.path )
        instance.delete()
    except UploadedFile.DoesNotExist:
        data = False

    return JFUResponse( request, data )



def get_results_dict(file_processing_tasks_qs):
    failed_tasks = []
    incomplete_tasks = []
    results = []

    for task in file_processing_tasks_qs.order_by('-id'):
        if task.status == ProcessingStatus.SUCCESS:
            results.extend(task.get_results())
        elif task.status == ProcessingStatus.ERROR:
            failed_tasks.append(task)
        else:
            incomplete_tasks.append(task)

    data_sources = {}
    graphs_to_load = []
    for result in results:
        if result.has_data_source():
            data_source_name = result.get_data_source_name()
            layout = result.get_layout()
            graphs_to_load.append((result.pk, data_source_name, layout))
            if data_source_name not in data_sources:
                data_sources[data_source_name] = result.get_data_source()
            
            
    
    return {"results" : results,
            "failed_tasks" : failed_tasks,
            "incomplete_tasks" : incomplete_tasks,
            "data_sources" : json.dumps(data_sources),
            "graphs_to_load" : json.dumps(graphs_to_load),}

def view_uploaded_file(request, project_id, uploaded_file_id):
    uploaded_file = get_object_or_404(models.UploadedFile, pk=uploaded_file_id, project_id=project_id)
    project_read_only_check(request, uploaded_file.project)

    if uploaded_file.needs_reference_set():
        initial_message = 'File type %s requires you select a reference.' %  uploaded_file.get_file_type_display()
        # reverse('', kwargs={'project_id', , uploaded_file_id'})
        center_panel_save_redirect_url = request.get_full_path()
        return edit_project(request, project_id, initial_message=initial_message, center_panel_save_redirect_url=center_panel_save_redirect_url)

    if request.POST:
        name_form = forms.UploadedFileNameForm(request.POST)
        if name_form.is_valid():
            name = name_form.cleaned_data['name']
            uploaded_file.name = name
            uploaded_file.save()
    else:
        name_form = forms.UploadedFileNameForm(initial={"name" : uploaded_file.name})
    
    context = {"project" : uploaded_file.project,
               "uploaded_file": uploaded_file,
               "name_form" : name_form,
    }

    uploaded_file_processing_tasks_qs = models.UploadedFileProcessingTask.objects.filter(uploaded_file=uploaded_file)
    results_dict = get_results_dict(uploaded_file_processing_tasks_qs)
    context.update(results_dict)
    
    return render_to_response('bgs/view_uploaded_file.html', RequestContext(request, context))


@require_POST
@json_view
def poll_task_results(request, project_id):
    task_ids = json.loads(request.POST["tasks"])

    tasks_qs = models.FileProcessingTask.objects.filter(project_id=project_id, id__in=task_ids)
    tasks = {}
    for(task_id, name, status, error_message) in tasks_qs.values_list("id", "name", "status", "error_message"):
        data = {'name' : str(name),
                'status' : str(status),}
        if status == ProcessingStatus.ERROR:
            data["error"] = str(error_message)
        tasks[task_id] = data
    return tasks

def task_results(request, project_id, task_id):
    task = get_subclass_or_404(models.FileProcessingTask, pk=task_id)
    task_permission_check(project_id, task)

    if task.status != ProcessingStatus.SUCCESS:
        msg = "Task %s status (%s) is not SUCCESS" % (task.pk, task.status)
        raise ValueError(msg)

    context = {"project" : task.project,
               "results" : task.get_results()}
    return render_to_response('bgs/results.html', RequestContext(request, context))

def project_read_only_check(request, project):
    if project.read_only and request.POST:
        msg = "POST forbidden for read only project"
        raise PermissionDenied(msg)

def task_permission_check(project_id, task):
    if task.project.pk != project_id:
        raise PermissionDenied("Wrong project_id '%s' for Task: %s" % (project_id, task.pk))

def result_permission_check(project_id, processing_result):
    task_permission_check(project_id, processing_result.file_processing_task)

def get_style_graph_permission_check(project_id, styled_graph_id):
    styled_graph = models.StyledGraph.objects.get_subclass(pk=styled_graph_id)
    result_permission_check(project_id, styled_graph.graph)
    return styled_graph

def get_result_permission_check(project_id, result_id):
    result = models.ProcessingResult.objects.get_subclass(pk=result_id)
    result_permission_check(project_id, result)
    return result

def get_combo_permission_check(project_id, combo_id):
    combo = get_object_or_404(models.Combo, pk=combo_id)
    if combo.project.pk != project_id:
        raise PermissionDenied("Wrong project_id '%s' for Combo: %s" % (project_id, combo.pk))
    return combo

def get_category_permission_check(project_id, category_id):
    category = get_object_or_404(models.GraphCategory, pk=category_id)
    result_permission_check(project_id, category.styled_graph.graph)
    return category


def load_styled_graph(request, project_id, styled_graph_id, hash_code=None):
    styled_graph = get_style_graph_permission_check(project_id, styled_graph_id)
    project = styled_graph.graph.file_processing_task.project
    project_read_only_check(request, project)
    if hash_code is not None:
        if styled_graph.hash_code != hash_code:
            msg = 'hash_code %s out of date' % hash_code
            raise ObjectDoesNotExist(msg)
    return styled_graph, project


def styled_graph_editor(request, project_id, styled_graph_id, hash_code):
    styled_graph, project = load_styled_graph(request, project_id, styled_graph_id, hash_code)

    context = {"project" : project,
               "styled_graph" : styled_graph,}
    editor_template = styled_graph.get_editor_template()
    return render_to_response(editor_template, RequestContext(request, context))


def download_csv(request, project_id, csv_result_id):
    csv_result = get_result_permission_check(project_id, csv_result_id)
    file_name = os.path.basename(csv_result.file_name)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="%s.csv"' % (file_name)

    df = pd.DataFrame.from_csv(csv_result.file_name, sep=None)
    df.to_csv(response)
    return response


@require_POST
@json_view
def create_combo(request, project_id):
    name = request.POST["name"]
    project = get_object_or_404(models.Project, pk=project_id)
    project_read_only_check(request, project)
    combo = models.Combo(project=project,
                         name=name)
    combo.save()
    return {"id" : combo.pk, "name" : name}

def view_auto_combo(request, project_id, result_type):
    project = get_object_or_404(models.Project, pk=project_id)
    uploaded_files_qs = get_uploaded_files_producing_result_type(project, result_type)

    if project.build is None: # Reference not set
        require_reference = uploaded_files_qs.filter(file_type__in=UploadedFileTypes.REQUIRES_REFERENCE)
        if require_reference:
            uploaded_files_info = ','.join(set([uploaded_file.get_file_type_display() for uploaded_file in require_reference]))
            initial_message = 'Uploaded files of type %s require a reference to be set.' %  uploaded_files_info
            center_panel_save_redirect_url = request.get_full_path()
            return edit_project(request, project_id, initial_message=initial_message, center_panel_save_redirect_url=center_panel_save_redirect_url)

    has_results = CSVResult.get_for_project_and_result_type(project_id, result_type).exists()
    incomplete_tasks_qs = models.FileProcessingTask.objects.filter(uploaded_file__in=uploaded_files_qs, status__in=ProcessingStatus.INCOMPLETE)
    
    context = {"project" : project,
               "has_results" : has_results,
               "incomplete_tasks" : incomplete_tasks_qs,
               "result_type" : result_type}
    return render_to_response('bgs/view_auto_combo.html', RequestContext(request, context))


def view_combo(request, project_id, combo_id):
    combo = get_combo_permission_check(project_id, combo_id)
    project_read_only_check(request, combo.project)

    if request.POST:
        old_combo_type = combo.combo_type
        form = forms.ComboForm(request.POST, instance=combo)
        if form.is_valid():
            combo = form.save()
            if old_combo_type != combo.combo_type:
                combo.combocolumn_set.all().delete()
    else:
        form = forms.ComboForm(instance=combo)

    context = {"project" : combo.project,
               "combo" : combo,
               "form" : form,
    }
    return render_to_response('bgs/view_combo.html', RequestContext(request, context))

@require_POST
def save_combo_columns(request, project_id, combo_id):
    combo = get_combo_permission_check(project_id, combo_id)
    project_read_only_check(request, combo.project)

    csv_result_ids_string = request.POST["csv_result_ids"]
    csv_result_ids = json.loads(csv_result_ids_string)
    
    # Delete existing
    combo.combocolumn_set.all().delete()
    
    for (i, csv_result_id) in enumerate(csv_result_ids): 
        csv_result = get_result_permission_check(project_id, csv_result_id)
        
        combo_column = ComboColumn(combo=combo,
                                   sort_order=i,
                                   csv_result=csv_result)
        combo_column.save()
        
    process_combo(combo)
    return HttpResponse()

def combo_columns(request, project_id, combo_id):
    combo = get_combo_permission_check(project_id, combo_id)

    (available_columns, selected_columns) = combo.get_columns()

    context = {"project" : combo.project,
               "combo" : combo,
               "available_columns" : available_columns,
               "selected_columns" : selected_columns,
    }
    return render_to_response('bgs/combo_columns.html', RequestContext(request, context))

def combo_grid(request, project_id, combo_id):
    combo = get_combo_permission_check(project_id, combo_id)
    context = {"project" : combo.project,
               "combo" : combo,
    }
    return render_to_response('bgs/combo_grid.html', RequestContext(request, context))

def combo_graphs(request, project_id, combo_id):
    combo = get_combo_permission_check(project_id, combo_id)

    
    context = {"project" : combo.project,
               "combo" : combo,}
    file_processing_tasks_qs = models.ComboProcessingTask.objects.filter(combo=combo)
    results_dict = get_results_dict(file_processing_tasks_qs)
    context.update(results_dict)

    return render_to_response('bgs/combo_graphs.html', RequestContext(request, context))

@json_view
def auto_combo_grid_config(request, project_id, result_type):
    ''' build a config suitable to pass to jqgrid constructor '''
    grid = AutoComboGrid(project_id, result_type)
    grid_config = grid.get_config(as_json=False)
    return grid_config

def auto_combo_grid_handler(request, project_id, result_type):
    grid = AutoComboGrid(project_id, result_type)
    return HttpResponse(grid.get_json(request), content_type="application/json")

def download_auto_combo_dataframe(request, project_id, result_type):
    response = HttpResponse(content_type='text/csv')
    result_type_name_lookup = dict(CSVResultType.CHOICES)
    response['Content-Disposition'] = 'attachment; filename="%s.csv"' % ("all_%s" % result_type_name_lookup[result_type])

    df = auto_combo_dataframe(project_id, result_type)
    df.to_csv(response)
    return response



@require_POST
@json_view
def delete_combo(request, project_id, combo_id):
    combo = get_combo_permission_check(project_id, combo_id)
    project_read_only_check(request, combo.project)
    combo.delete()
    return {"id" : combo_id}

@json_view
def combo_grid_config(request, project_id, combo_id):
    ''' build a config suitable to pass to jqgrid constructor '''
    print "combo_grid_config"
    combo = get_combo_permission_check(project_id, combo_id)
    grid = ComboGrid(combo)
    grid_config = grid.get_config(as_json=False)
    return grid_config


def combo_grid_handler(request, project_id, combo_id):
    combo = get_combo_permission_check(project_id, combo_id)
    grid = ComboGrid(combo)
    return HttpResponse(grid.get_json(request), content_type="application/json")

def download_combo_dataframe(request, project_id, combo_id):
    combo = get_combo_permission_check(project_id, combo_id)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="%s.csv"' % (combo.name)

    df = combo.get_dataframe()
    df.to_csv(response)
    return response


def graph_editor_tab(request, project_id, styled_graph_id):
    styled_graph, project = load_styled_graph(request, project_id, styled_graph_id)

    form_class = get_styled_graph_form_class(styled_graph)
    styled_graph_form = form_class(request.POST or None, instance=styled_graph)
    
    if request.POST:
        if styled_graph_form.is_valid():
            styled_graph = styled_graph_form.save()

    context = {"project" : project,
               "styled_graph" : styled_graph,
               "styled_graph_form" : styled_graph_form}
    return render_to_response("bgs/graph_editors/graph_editor_tab.html", RequestContext(request, context))


def graph_category_tab(request, project_id, styled_graph_id):
    styled_graph, project = load_styled_graph(request, project_id, styled_graph_id)

    categories_dict = styled_graph.get_categories_dict()
    context = {"project" : project,
               "project_id" : project_id,
               "styled_graph" : styled_graph,
               "existing_categories" : json.dumps(categories_dict),}
    return render_to_response("bgs/graph_editors/graph_category_tab.html", RequestContext(request, context))


@require_POST
@json_view
def create_category(request, project_id, styled_graph_id):
    styled_graph, project = load_styled_graph(request, project_id, styled_graph_id)
    project_read_only_check(request, project)

    name = request.POST["name"]
    category = models.GraphCategory(styled_graph=styled_graph,
                                    name=name)
    category.save()
    return {"id" : category.pk, "name" : name}


def create_category_formset(post, category):
    widgets = {"name" : TextInput(attrs={'placeholder': 'New Group...'})}
    if category.cmap:
        widgets['color'] = HiddenInput()

    graph_groups_fields = ['graph_category', 'name', 'color']
    GraphGroupsFormSet = inlineformset_factory(models.GraphCategory,
                                               models.GraphGroup,
                                               can_delete=True,
                                               widgets=widgets,
                                               max_num=10,
                                               extra=3,
                                               formset=forms.GroupNameFormSet,
                                               fields=graph_groups_fields) 
    return GraphGroupsFormSet(post, instance=category)
    

def view_category(request, project_id, category_id):
    # Taken from matplotlib
    def rgb2hex(rgb):
        'Given an rgb or rgba sequence of 0-1 floats, return the hex string'
        a = '#%02x%02x%02x' % tuple([int(np.round(val * 255)) for val in rgb[:3]])
        return a
    
    category = get_category_permission_check(project_id, category_id)
    project = category.get_project()
    project_read_only_check(request, project)

    category_form = forms.GraphCategoryForm(request.POST or None, instance=category)
    groups_formset = create_category_formset(request.POST or None, category)
    if request.POST:
        if category_form.is_valid() and groups_formset.is_valid():
            category = category_form.save()

            # We need to add 3 more empties, and remove deleted, easier to just reload from data
            groups_formset.save()
            groups_formset = create_category_formset(None, category)

    context = {"project" : project,
               "category" : category,
               "category_form" : category_form,
               "groups_form" : groups_formset,}

    if category.cmap:
        (_, group_colors) = category.get_sample_groups_and_colors()
        hex_colors = OrderedDict()
        for (group_name, rgb) in group_colors.iteritems():
            hex_colors[group_name] = rgb2hex(rgb)

        context["group_colors"] = json.dumps(hex_colors)

    return render_to_response('bgs/graph_editors/view_category.html', RequestContext(request, context))


@require_POST
@json_view
def delete_category(request, project_id, category_id):
    category = get_category_permission_check(project_id, category_id)
    project_read_only_check(request, category.get_project())
    category.delete()
    return {"id" : category_id}

def graph_samples_tab(request, project_id, styled_graph_id):
    styled_graph, project = load_styled_graph(request, project_id, styled_graph_id)

    form = forms.GraphCategoryChoiceForm(styled_graph=styled_graph)

    context = {"project" : project,
               "project_id" : project_id,
               "form" : form,}
    return render_to_response("bgs/graph_editors/graph_samples_tab.html", RequestContext(request, context))


def view_graph_samples(request, project_id, category_id):
    category = get_category_permission_check(project_id, category_id)
    project = category.get_project()
    project_read_only_check(request, project)

    styled_graph = category.styled_graph

    sample_names = {sample_id : name for (sample_id, name) in styled_graph.graphsample_set.all().values_list("id", "name")}
    widgets = {f : HiddenInput() for f in ["graph_sample", "graph_category"]}

    graph_sample_group_relationship_fields = ['graph_sample', 'graph_category', 'graph_group']
    CategorySampleGroupRelationshipFormSet = inlineformset_factory(models.GraphCategory, models.GraphSampleGroupRelationship,
                                                                   can_delete=False,
                                                                   widgets=widgets,
                                                                   extra=0,
                                                                   formset=forms.GraphSampleGroupRelationshipFormSet,
                                                                   fields=graph_sample_group_relationship_fields) 
    category_formset = CategorySampleGroupRelationshipFormSet(request.POST or None, instance=category)
    if request.POST:
        if category_formset.is_valid():
            category_formset.save()

    context = {"project" : project,
               "project_id" : project_id,
               "category" : category,
               "category_formset" : category_formset,
               "sample_names" : json.dumps(sample_names),
    }
    return render_to_response("bgs/graph_editors/view_graph_samples.html", RequestContext(request, context))


def lost_projects(request):
    context = {}
    if request.POST:
        if request.is_ajax():
            print "is ajax..."
        
        form = forms.EmailForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            projects_qs = models.Project.objects.filter(email=email)
            num_projects = projects_qs.count()
            message = "%d projects found for email '%s'." % (num_projects, email)
            if num_projects:
                try:
                    context = {"projects" : projects_qs,
                               "hostname" : "http://biographserv.com"}
                    email_body = render_to_string("bgs/email/lost_projects.txt", context)
    
                    send_mail('BioGraphServ project links', email_body, settings.EMAIL_FROM,
                              [email], fail_silently=False)
                    message += " Email sent."
                except Exception as e:
                    message = "There was a problem sending your email: " + str(e)
            else:
                message += " No email sent."
            context["message"] = message
            form = None # Dont' show...
    else:
        form = forms.EmailForm()

    context["form"] = form
    return render_to_response('bgs/lost_projects.html', RequestContext(request, context))


def docs(request, doc_name):
    documentation_url = "/static/html/%s.html" % doc_name
    context = {"documentation_url" : documentation_url}
    return render_to_response('bgs/docs.html', RequestContext(request, context))

