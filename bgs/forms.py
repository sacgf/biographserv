'''
Created on 22/11/2014

@author: dlawrence
'''

from django import forms
from django.core.validators import validate_email
from django.forms.models import BaseInlineFormSet
from django.forms.widgets import TextInput, Select
from smart_selects.form_fields import ChainedModelChoiceField

from bgs import models


class ProjectForm(forms.Form):
    organism = forms.ModelChoiceField(queryset=models.Organism.objects.all())
    build = ChainedModelChoiceField('bgs', 'Build', 'organism', 'organism',
                                    'bgs', 'ProjectForm', 'build',
                                    False, False, queryset=models.Build.objects.all())
    name = forms.CharField(models.Project._meta.get_field('name').max_length, required=False) #@UndefinedVariable
    email = forms.CharField(models.Project._meta.get_field('email').max_length, required=False) #@UndefinedVariable
    description = forms.CharField(widget=forms.Textarea, required=False)

class ComboForm(forms.ModelForm):
    class Meta:
        exclude = ('project', 'file_name')
        model = models.Combo

class EmailForm(forms.Form):
    email = forms.CharField(models.Project.MAX_EMAIL_LENGTH, required=True)

    def validate(self, value):
        super(EmailForm, self).validate(value)
        validate_email(self.email)
        
class UploadedFileNameForm(forms.Form):
    name = forms.CharField()

COLORMAP_CHOICES = [('jet', 'jet'),
                    ('hot', 'hot'),
                    ('RdYlGn_r', 'green-red'),
                    ('RdYlGn', 'red-green'),
                    ('gray', 'gray'),
                    ('gist_heat', 'heat')]

class GraphForm(forms.ModelForm):
    class Meta:
        exclude = ('graph', 'hash_code', 'number_of_colors', 'has_colormap')
        model = models.StyledGraph

        widgets = {'title' : TextInput(),
                   'cmap' : Select(choices=COLORMAP_CHOICES),
                   'title' : TextInput(),
                   'xlabel' : TextInput(),
                   'ylabel' : TextInput(),
        }


class SVDGraphForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        styled_graph = kwargs["instance"]
        super(SVDGraphForm, self).__init__(*args, **kwargs)
        category = self.fields['category']
        category.required = False
        category.queryset = styled_graph.graphcategory_set.all()

    class Meta:
        exclude = ('graph', 'hash_code', 'number_of_colors', 'has_colormap')
        model = models.SVDStyledGraph

        widgets = {'title' : TextInput(),
                   'cmap' : Select(choices=COLORMAP_CHOICES),
                   'title' : TextInput(),
                   'xlabel' : TextInput(),
                   'ylabel' : TextInput(),
        }



def get_styled_graph_form_class(styled_graph):
    if isinstance(styled_graph, models.SVDStyledGraph):
        return SVDGraphForm
    else:
        return GraphForm

class GraphCategoryChoiceForm(forms.Form):
    category = forms.ModelChoiceField(empty_label=None, queryset=models.GraphCategory.objects.all())

    def __init__(self, *args, **kwargs):
        styled_graph = kwargs.pop("styled_graph")
        super(GraphCategoryChoiceForm, self).__init__(*args, **kwargs)

        self.fields['category'].queryset = styled_graph.graphcategory_set.all()
        
        
class GraphSampleGroupRelationshipFormSet(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super(GraphSampleGroupRelationshipFormSet, self).__init__(*args, **kwargs)
        self.queryset = self.queryset.order_by("graph_sample__name")
        graph_category = kwargs["instance"]
        
        for form in self.forms:
            queryset = form.fields['graph_group'].queryset.filter(graph_category=graph_category)
            form.fields['graph_group'].queryset = queryset

class GraphCategoryForm(forms.ModelForm):
    class Meta:
        exclude = ('styled_graph', )
        model = models.GraphCategory

        widgets = {'name' : TextInput(),
                   'cmap' : Select(choices=[('', 'Use Individual Group Colors')] + COLORMAP_CHOICES),
        }    

class GroupNameFormSet(forms.models.BaseInlineFormSet):
    def clean(self):
        super(GroupNameFormSet, self).clean()
        existing_names = set()

        for form in self.forms:
            try:
                cleaned_data = form.cleaned_data
                if cleaned_data and not cleaned_data.get('DELETE', False):
                    name = cleaned_data.get("name")
                    if name:
                        if name in existing_names:
                            msg = "Name '%s' is not unique" % name
                            form._errors['name'] = self.error_class([msg])
                            del cleaned_data['name']
                        else:
                            existing_names.add(name)
            except AttributeError:
                # annoyingly, if a subform is invalid Django explicity raises
                # an AttributeError for cleaned_data
                pass

