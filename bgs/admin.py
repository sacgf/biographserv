from django.contrib import admin
from bgs import models

# Register your models here.
admin.site.register(models.Organism)
admin.site.register(models.AnnotationGroup)
admin.site.register(models.Build)
admin.site.register(models.Project)
admin.site.register(models.UploadedFile)
