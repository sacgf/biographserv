


from collections import OrderedDict
from django.db import models
from django.db.models.aggregates import Min
from django.db.models.deletion import SET_NULL
import grp
import hashlib
from model_utils.managers import InheritanceManager
from operator import attrgetter
import os
from smart_selects.db_fields import ChainedForeignKey
import stat

from bgs.models_enum import ComboType, CSVResultType, UploadedFileTypes, \
    GraphType
from bgs.plotly_json import plotly_data, plotly_layout
from bgs.utils.file_utils import mk_path, name_from_file_name
from bgs.utils.utils import Lazy
from biographserv import settings
import pandas as pd
from reference_server.reference.igenomes_reference import IGenomesReference


class Organism(models.Model):
    species_name = models.CharField(max_length=256)
    common_name = models.CharField(max_length=256)
    def __unicode__(self):
        return "%s (%s)" % (self.species_name, self.common_name)

class AnnotationGroup(models.Model):
    name = models.CharField(max_length=256)
    def __unicode__(self):
        return self.name

class Build(models.Model):
    organism = models.ForeignKey(Organism)
    annotation_group = models.ForeignKey(AnnotationGroup)
    name = models.CharField(max_length=256)

    def get_igenomes_reference(self):
        return IGenomesReference(self.organism.species_name, self.annotation_group.name, self.name)

    def __unicode__(self):
        return "%s/%s" % (self.annotation_group, self.name)

class Project(models.Model):
    MAX_EMAIL_LENGTH=256
    uuid = models.CharField(max_length=32, primary_key=True)
    organism = models.ForeignKey(Organism, null=True)
#    annotation_group = models.ForeignKey(AnnotationGroup, null=True)
    build = ChainedForeignKey(Build,
                              chained_field='organism',
                              chained_model_field='organism',
                              show_all=False,
                              auto_choose=True,
                              null=True)
    name = models.CharField(max_length=100)
    description = models.TextField()
    email = models.CharField(max_length=MAX_EMAIL_LENGTH)
    read_only = models.BooleanField(default=False)
    
    @property
    def date(self):
        # Earliest uploaded file
        return self.uploadedfile_set.all().aggregate(Min('date')).values()[0]

    def get_combos(self):
        combo_dicts = []
        for (combo_id, name) in self.combo_set.all().order_by("id").values_list("id", "name"):
            combo_data = {"id" : combo_id, "name" : name}
            combo_dicts.append(combo_data)
        return combo_dicts

    def create_default_genomic_ranges(self):
        GenomicRange.objects.create(project=self,
                                name='Promoter',
                                anchor=GenomicAnchor.TSS,
                                upstream=1000,
                                downstream=1000,
                                max_size=3000,
                                step_size=100,)

        GenomicRange.objects.create(project=self,
                                    name='Gene Body',
                                    anchor=GenomicAnchor.GENE_START_END,
                                    upstream=0,
                                    downstream=0,
                                    max_size=20000,
                                    step_size=500,)


    def __unicode__(self):
        name = self.name or self.pk
        if self.build:
            name = "%s (%s)" % (name, self.build)
        return name


class Combo(models.Model):
    project = models.ForeignKey(Project)
    name = models.CharField(max_length=100)
    combo_type = models.CharField(max_length=1, choices=ComboType.COMBO_TYPE_CHOICE, null=True, blank=True)
    file_name = models.TextField()
    
    def get_columns(self):
        if self.combo_type is None:
            return ([], []) 
        
        csv_results_qs = CSVResult.get_for_project(self.project)
        if self.combo_type == ComboType.REGION_STATS:
            columns_qs = csv_results_qs.filter(result_type=CSVResultType.REGION_STATS)
        else:
            result_types = [CSVResultType.GENE_SCORES]
            result_types.extend(CSVResultType.UPLOADED_CSVRESULTS)
            columns_qs = csv_results_qs.filter(result_type__in=result_types)
        
        selected_column_ids = self.combocolumn_set.all().values_list("csv_result_id", flat=True)
        selected_columns = CSVResult.objects.filter(id__in=selected_column_ids).order_by("combocolumn__sort_order")
        available_columns = columns_qs.exclude(id__in=selected_column_ids)
        return (available_columns, selected_columns)

    def get_dataframe(self):
        dataframes = []
        for cc in self.combocolumn_set.all().order_by("sort_order"):
            cc_df = cc.csv_result.get_dataframe()
            dataframes.append(cc_df)
            
        df = pd.concat(dataframes, axis=1)
        return df

    @staticmethod
    def get_csv_result_type_mappings():
        ''' Basically a way to have expression types map the same '''
        mappings = {k : k for (k,_) in CSVResultType.CHOICES}
        mappings[CSVResultType.SINGLE_SAMPLE_EXPRESSION] = 'Expression'
        mappings[CSVResultType.MULTI_SAMPLE_EXPRESSION] = 'Expression'
        return mappings

    def get_column_types_and_homogeneity(self):
        ''' returns (num_column_types, distinct_column_types, is_homogenous) '''

        mappings = self.get_csv_result_type_mappings()
        column_types = list(self.combocolumn_set.all().values_list('csv_result__result_type', flat=True))

        num_column_types = len(column_types)
        distinct_column_types = set(mappings[t] for t in column_types) 
        is_homogenous = len(distinct_column_types) <= 1

        return (num_column_types, distinct_column_types, is_homogenous)
        
    def is_multi_expression(self):
        (num_column_types, distinct_column_types, is_homogenous) = self.get_column_types_and_homogeneity()
        return num_column_types > 1 and is_homogenous and "Expression" in distinct_column_types

    
    def __unicode__(self):
        return "Combo: %s%s" % (self.name or '', self.project)

class GenomicAnchor(object):
    TSS = 'P'
    GENE_START_END = 'G'
    GENOMIC_ANCHOR_CHOICE = (
        (TSS, 'TSS'),
        (GENE_START_END, 'Gene Start/End')
    )

class GenomicRange(models.Model):
    project = models.ForeignKey(Project)
    name = models.TextField()
    anchor = models.CharField(max_length=1, choices=GenomicAnchor.GENOMIC_ANCHOR_CHOICE)
    upstream = models.IntegerField()
    downstream = models.IntegerField()
    max_size = models.IntegerField()
    step_size = models.IntegerField()


def get_processing_dir(project_id):
    processing_dir = os.path.join(settings.PROCESSOR_OUTPUT_DIR, str(project_id))
    mk_path(processing_dir)
    posix_stat = os.stat(processing_dir)

    if settings.GROUP_NAME:
        try:
            uid = posix_stat.st_uid
            gid = grp.getgrnam(settings.GROUP_NAME).gr_gid
    #        print "Changing permissions:"
            os.chown(processing_dir, uid, gid)
    #        print "Changing group ownership:"
            os.chmod(processing_dir, posix_stat.st_mode | stat.S_IWGRP)
        except Exception as e:
            print e # Ignore
#    posix_stat = os.stat(processing_dir)
#    mode = posix_stat.st_mode
    
#    print "%s mode: %d" % (processing_dir, mode)
#    if not stat.S_IWGRP & mode:
#        print "Not writable by GROUP" 
#    if not stat.S_IWOTH & mode:
#        print "Not writable by others" 
    
    return processing_dir



class UploadedFile(models.Model):
    project = models.ForeignKey(Project)
    name = models.TextField()
    uploaded_file = models.FileField(upload_to='uploads', max_length=256)
    file_type = models.CharField(max_length=1, choices=UploadedFileTypes.UPLOADED_FILES_CHOICE)
    date = models.DateTimeField()
    def get_file(self):
        ''' returns the actual file on the server '''
        return self.uploaded_file.file.file

    def uses_reference(self):
        return UploadedFileTypes.requires_reference(self.file_type)

    def needs_reference_set(self):
        return self.uses_reference() and self.project.build is None

    def __unicode__(self):
        return "%s (%s)" % (self.name, self.get_file_type_display())

class ProcessingStatus(object):
    CREATED = 'C'
    PROCESSING = 'P'
    ERROR = 'E'
    SUCCESS = 'S'
    STATUS_CHOICE = (
        (CREATED, 'Created'),
        (PROCESSING, 'Processing'),
        (ERROR, 'Error'),
        (SUCCESS, 'Success'),
    )
    INCOMPLETE = (CREATED, PROCESSING)


class FileProcessingTask(models.Model):
    ''' This represents the celery execution of a Processor '''
    objects = InheritanceManager()
    project = models.ForeignKey(Project, null=True)
    name = models.TextField()
    status = models.CharField(max_length=1, choices=ProcessingStatus.STATUS_CHOICE)
    error_message = models.TextField()
    created_date = models.DateTimeField()
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)
    items_processed = models.IntegerField(default=0)
    celery_task = models.CharField(max_length=36, null=True)

    @property
    def processing_seconds(self):
        if self.start_date and self.end_date:
            seconds = (self.end_date - self.start_date).total_seconds()
        else:
            seconds = None
        return seconds
    
    def get_base_name(self):
        ''' Used for any produced files '''
        return NotImplementedError()
    
    def uses_reference(self):
        return False
    
    def get_reference(self):
        return self.project.build.get_igenomes_reference()
    
    def get_results(self):
        return self.processingresult_set.select_subclasses().order_by('-id')


class UploadedFileProcessingTask(FileProcessingTask):
    uploaded_file = models.ForeignKey(UploadedFile)

    def get_base_name(self):
        ''' Used for any produced files '''
        return name_from_file_name(self.uploaded_file.uploaded_file.path)
    
    def uses_reference(self):
        return self.uploaded_file.uses_reference()
    
    def __unicode__(self):
        return "UploadedFileProcessingTask: %s (%s)" % (self.name, self.status)


class ComboProcessingTask(FileProcessingTask):
    combo = models.ForeignKey(Combo)

    def get_base_name(self):
        return "combo_%s" % self.combo.name

    def __unicode__(self):
        return "ComboProcessingTask: %s (%s)" % (self.name, self.status)


class ProcessingResultType(object):
    CSV = 'C'
    GRAPH = 'G'
    CHOICES = (
        (CSV, 'CSV'),
        (GRAPH, 'Graph')               
    )


class ProcessingResult(models.Model):
    objects = InheritanceManager()
    name = models.TextField()
    file_processing_task = models.ForeignKey(FileProcessingTask)

    @property
    def processing_task(self):
        return FileProcessingTask.objects.get_subclass(pk=self.file_processing_task.pk)

    def has_data_source(self):
        return False

    def get_data_source_name(self):
        return None

    def get_data_source(self):
        return None

    def get_display_type(self):
        raise NotImplementedError("ProcessingResult is an abstract class")

    def is_graph(self):
        return self.get_display_type() == ProcessingResultType.GRAPH

    def is_csv(self):
        return self.get_display_type() == ProcessingResultType.CSV

    def __unicode__(self):
        return "ProcessingResult(%s)" % self.pk

class Graph(ProcessingResult):
    processing_result = models.ForeignKey(ProcessingResult, related_name='graphprocessingresult', null=True)
    graph_type = models.CharField(max_length=2, choices=GraphType.CHOICES)

    def get_display_type(self):
        return ProcessingResultType.GRAPH

    @staticmethod
    def get_for_project(project_id):
        return Graph.objects.filter(file_processing_task__uploaded_file__project=project_id)

    @Lazy
    def get_processing_result(self):
        return ProcessingResult.objects.get_subclass(pk=self.processing_result_id)

    def has_data_source(self):
        return True

    def get_data_source_name(self):
        data_source_name = None
        if self.processing_result:
            data_source_name = self.get_processing_result.get_data_source_name()
        return data_source_name

    def get_data_source(self):
        pr = self.get_processing_result
        return plotly_data.get_csv_data_source(pr.file_name, pr.result_type, self.graph_type)

    def get_layout(self):
        return plotly_layout.get_graph_layout(self)

    def __unicode__(self):
        return "Graph(%s): Type: %s Data: %s" % (self.pk, self.get_graph_type_display(), self.get_data_source_name())


class CSVResult(ProcessingResult):
    file_name = models.TextField()
    result_type = models.CharField(max_length=1, choices=CSVResultType.CHOICES)

    def get_display_type(self):
        return ProcessingResultType.CSV

    def get_dataframe(self):
        loader = CSVResultType.LOADERS[self.result_type]
        name = str(self)
        return loader(self.file_name, name=name)

    def get_data_source_name(self):
        name_components = []
        if self.name:
            name_components.append(self.name)
        name_components.append(str(self.pk))
            
        return "_".join(name_components).replace(" ", "_")

    @staticmethod
    def get_for_project(project_id):
        return CSVResult.objects.filter(file_processing_task__uploaded_file__project=project_id)

    @staticmethod
    def get_for_project_and_result_type(project_id, result_type):
        return CSVResult.objects.filter(file_processing_task__uploaded_file__project_id=project_id, result_type=result_type)

    def __unicode__(self):
        name = self.processing_task.get_base_name()
        if self.name:
            name = "%s: %s" % (name, self.name)
        return name


class CSVColumn(models.Model):
    csv = models.ForeignKey(CSVResult)
    sort_order = models.IntegerField()
    column_name = models.TextField()


class ComboColumn(models.Model):
    combo = models.ForeignKey(Combo)
    sort_order = models.IntegerField()
    csv_result = models.ForeignKey(CSVResult)


class StyledGraph(models.Model):
    objects = InheritanceManager()
    graph = models.ForeignKey(Graph)
    hash_code = models.CharField(max_length=40, null=True)

    # These we copy from SerialisableGraph
    GRAPH_FIELDS = ['title',
                    'cmap',
                    'title',
                    'xlabel',
                    'ylabel',
                    'top_axis',
                    'bottom_axis',
                    'left_axis',
                    'right_axis',
                    'xgrid',
                    'ygrid',
    ]
    
    title = models.TextField(null=True, blank=True)
    color = models.CharField(max_length=7, default='#ff0000')
    cmap = models.TextField(null=True, blank=True)
    xlabel = models.TextField(null=True, blank=True)
    ylabel = models.TextField(null=True, blank=True)
    top_axis = models.BooleanField(default=True)
    bottom_axis = models.BooleanField(default=True)
    left_axis = models.BooleanField(default=True)
    right_axis = models.BooleanField(default=True)
    xgrid = models.BooleanField(default=False)
    ygrid = models.BooleanField(default=False)

    number_of_colors = models.IntegerField(default=0)
    has_colormap = models.BooleanField(default=False)

    def get_project_id(self):
        return self.graph.file_processing_task.project.pk

    def get_filename(self, image_type):
        file_name = self.hash_code + '.' + image_type
        return os.path.join(get_processing_dir(self.get_project_id()), "style_graphs", str(self.pk), file_name)

    def _get_hashcode_fields(self):
        return [str(getattr(self, f)) for f in self.GRAPH_FIELDS]

    def _get_hashcode(self):
        sha1 = hashlib.sha1()
        sha1.update(str(self.processing_result.pk))
        for f in self._get_hashcode_fields():
            sha1.update(f)

        sha1.update(self.color)
        return sha1.hexdigest()

    def save(self, *args, **kwargs):
        self.hash_code = self._get_hashcode()
        super(ProcessingResult, self).save(*args, **kwargs)

    def has_colors(self):
        return self.number_of_colors > 0

    def get_categories_dict(self):
        categories_dicts = []
        for (category_id, name) in self.graphcategory_set.all().order_by("id").values_list("id", "name"):
            category_data = {"id" : category_id, "name" : name}
            categories_dicts.append(category_data)
        return categories_dicts

    def get_editor_template(self):
        return 'bgs/graph_editors/styled_graph_editor.html'

    @staticmethod
    def get_for_project(project_id):
        return StyledGraph.objects.filter(processing_result__file_processing_task__project=project_id)


class GraphSample(models.Model):
    styled_graph = models.ForeignKey(StyledGraph)
    name = models.TextField()
    color = models.CharField(max_length=7, null=True)

    def __unicode__(self):
        return self.name

class GraphCategory(models.Model):
    styled_graph = models.ForeignKey(StyledGraph)
    name = models.TextField()
    cmap = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        super(GraphCategory, self).save(*args, **kwargs)
        
        # Make sure we have a GSGR for each sample
        for graph_sample in self.styled_graph.graphsample_set.all():
            GraphSampleGroupRelationship.objects.get_or_create(graph_sample=graph_sample,
                                                               graph_category=self)

    def get_project(self):
        return self.graph.processing_result.file_processing_task.project

    def get_sample_groups_and_colors(self):
        graph_sample_qs = self.graphsamplegrouprelationship_set.all()
        sample_groups = OrderedDict()
        groups = set()
        for gsgr in graph_sample_qs:
            groups.add(gsgr.graph_group)
            sample_groups[gsgr.graph_sample.name] = gsgr.graph_group.name

        groups = sorted(groups, key=attrgetter('id'))
        
        # TODO: Handle cmap
        # if self.cmap:
            
        colors = [g.color for g in groups]
        
        group_colors = OrderedDict()
        for (i, graph_group) in enumerate(groups):
            group_colors[graph_group.name] = colors[i]

        return (sample_groups, group_colors)


    def __unicode__(self):
        return self.name

class GraphGroup(models.Model):
    graph_category = models.ForeignKey(GraphCategory)
    name = models.TextField()
    color = models.CharField(max_length=7, null=True)

    def __unicode__(self):
        return self.name

class GraphSampleGroupRelationship(models.Model):
    graph_sample = models.ForeignKey(GraphSample)
    graph_category = models.ForeignKey(GraphCategory)
    graph_group = models.ForeignKey(GraphGroup, null=True, on_delete=SET_NULL)


class SVDStyledGraph(StyledGraph):
    category = models.ForeignKey(GraphCategory, null=True)


def auto_combo_dataframe(project_id, result_type):
    qs = CSVResult.get_for_project_and_result_type(project_id, result_type)
    dataframes = []
    for csv_result in qs:
        csv_result_df = csv_result.get_dataframe()
        dataframes.append(csv_result_df)
        
    df = pd.concat(dataframes, axis=1)
    df = df.fillna(0)
    return df

