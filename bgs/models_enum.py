'''
Created on 05/12/2016

@author: dlawrence
'''
from bgs import genomics
from bgs.utils.django_utils import delimited_file_has_header
from bgs.utils.file_utils import get_extension
from bgs.utils.pandas_utils import load_single_column_df, load_multi_column_df, \
    load_series


class GraphType(object):
    BAR = 'BR'
    BOXPLOT = 'BO'
    CHROM_DENSITY = 'CD'
    CHROM_INTERVALS = 'CI'
    CORRELATION_MATRIX = 'CM'
    LINE = 'LN'
    LABELLED_SCATTER = 'LS'
    SCATTER = 'SC'
    TEXT_SCATTER = 'TS'
    VOLCANO = 'VO'
    CHOICES = (
        (BAR, 'Bar'),
        (BOXPLOT, 'Boxplot'),
        (CHROM_DENSITY, 'Chrom Density'),
        (CHROM_INTERVALS, 'Chrom Intervals'),
        (CORRELATION_MATRIX, 'Correlation Matrix'),
        (LINE, 'Line'),
        (LABELLED_SCATTER, 'Labelled Scatter'),
        (TEXT_SCATTER, 'Text Scatter'),
        (SCATTER, 'Scatter'),
        (VOLCANO, 'Volcano'),
    )


class CSVResultType(object):
    CHROMOSOME_BIN_SCORES = 'B'
    CORRELATION_MATRIX = 'X'
    CUFFDIFF = 'C'
    GENE_SCORES = 'G'
    MULTI_SAMPLE_EXPRESSION = 'M'
    OTHER = 'O'
    READ_COUNTS = 'T' # t for tags
    REGION_STATS = 'R'
    SINGLE_SAMPLE_EXPRESSION = 'S'
    SVD_SCREE = 'E'
    SVD_VT = 'V'
    VCF_INFO_COUNTS = 'I'

    CHOICES = (
        (CHROMOSOME_BIN_SCORES, 'Chromosome Bin Scores'),
        (CORRELATION_MATRIX, 'Correlation Matrix'),
        (CUFFDIFF, 'Uploaded CuffDiff'),
        (GENE_SCORES, 'Bed Gene Scores'),
        (MULTI_SAMPLE_EXPRESSION, 'Uploaded Multi Sample Expressions'),
        (OTHER, 'Other'),
        (READ_COUNTS, 'Read Counts'),
        (REGION_STATS, 'Region Stats'),
        (SINGLE_SAMPLE_EXPRESSION, 'Uploaded Single Sample Expression'),
        (SVD_SCREE, 'SVD Scree'),
        (SVD_VT, 'SVD_VT'),
        (VCF_INFO_COUNTS, 'VCF Info Counts'),
    )
    # These get results automatically made to download
    UPLOADED_CSVRESULTS = set([CUFFDIFF, SINGLE_SAMPLE_EXPRESSION, MULTI_SAMPLE_EXPRESSION])
    LOADERS = {CUFFDIFF : load_multi_column_df,
               CORRELATION_MATRIX : load_multi_column_df,
               GENE_SCORES : load_single_column_df,
               MULTI_SAMPLE_EXPRESSION : load_multi_column_df,
               READ_COUNTS : load_series,
               REGION_STATS : load_single_column_df,
               SINGLE_SAMPLE_EXPRESSION : load_single_column_df,
               SVD_SCREE : load_series,
               SVD_VT : load_multi_column_df,
    }



class ComboType(object):
    REGION_STATS = 'R'
    GENES = 'G'
    OTHER = 'O'
    COMBO_TYPE_CHOICE = (
        (REGION_STATS, 'Region Stats'),
        (GENES, 'Genes'),
        (OTHER, 'Other'),
    )


class UploadedFileTypes(object):
    BED = 'B'
    VCF = 'V'
    CUFFDIFF = 'C'
    EXPRESSION = 'E'
    SINGLE_SAMPLE_EXPRESSION = 'S'
    MULTI_SAMPLE_EXPRESSION = 'M'
    UPLOADED_FILES_CHOICE = (
        (BED, 'BED'),
        (VCF, 'VCF'),
        (CUFFDIFF, 'CuffDiff'),
        (SINGLE_SAMPLE_EXPRESSION, 'Expression (single sample)'),
        (MULTI_SAMPLE_EXPRESSION, 'Expression (multi-sample)'),
    )

    EXTENSIONS = {BED : ['bed', 'broadPeak', 'narrowPeak'],
                  VCF : ['vcf'],
                  EXPRESSION : ['tsv', 'csv'],
                  CUFFDIFF : ['diff']}

    REQUIRES_REFERENCE = {BED}
    REQUIRES_GENOMIC_RANGES = {BED}

    @classmethod
    def get_file_type_from_extension(cls, uploaded_file):
        extension_lookup = cls.get_extension_lookup()

        filename = uploaded_file.name
        filename, open_file = genomics.handle_open_file_gzip(filename, uploaded_file.file)
        ext = get_extension(filename)
        try:
            file_type = extension_lookup[ext]
        except KeyError as e:
            print e
            msg = "Can't handle files with extension '%s'" % ext
            raise ValueError(msg)
            
        if file_type == UploadedFileTypes.EXPRESSION:
            file_type = cls.determine_expression_type(filename, open_file) 
        
        return file_type

    @classmethod
    def get_extension_lookup(cls):
        extension_lookup = {}
        for (file_type, extensions) in cls.EXTENSIONS.iteritems():
            for ext in extensions:
                extension_lookup[ext] = file_type
        return extension_lookup

    @classmethod
    def determine_expression_type(cls, file_name, open_file):
        (has_header, num_columns, _) = delimited_file_has_header(open_file)
        if num_columns == 2 and not has_header:
            return cls.SINGLE_SAMPLE_EXPRESSION
        elif num_columns > 2 and has_header:
            return cls.MULTI_SAMPLE_EXPRESSION
        
        msg = "Could not determine expression type of %s header: %s, columns: %d" % (file_name, has_header, num_columns)
        raise ValueError(msg)

    @classmethod
    def requires_reference(cls, uploaded_file_type):
        return uploaded_file_type in cls.REQUIRES_REFERENCE

