from django.conf.urls import patterns, url
from bgs import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^view_project/(?P<project_id>.+)/$', views.view_project, name='view_project'),
    url(r'^edit_project/(?P<project_id>.+)/$', views.edit_project, name='edit_project'),
    url(r'^delete_project/(?P<project_id>.+)/$', views.delete_project, name='delete_project'),
    url(r'^download_project/(?P<project_id>.+)/$', views.download_project, name='download_project'),

    url( r'upload/', views.upload, name = 'jfu_upload' ),
    url( r'^delete/(?P<pk>\d+)$', views.upload_delete, name = 'jfu_delete' ),
    url(r'^view_uploaded_file/(?P<project_id>.+)/(?P<uploaded_file_id>\d+)$', views.view_uploaded_file, name='view_uploaded_file'),
    url(r'^poll_task_results/(?P<project_id>.+)/$', views.poll_task_results, name='poll_task_results'),
    url(r'^results/(?P<project_id>.+)/(?P<task_id>\d+)$', views.task_results, name='task_results'),
    url(r'^download_csv/(?P<project_id>.+)/(?P<csv_result_id>\d+)/$', views.download_csv, name='download_csv'),
    url(r'^styled_graph_editor/container/(?P<project_id>.+)/(?P<styled_graph_id>\d+)/(?P<hash_code>.+)/$', views.styled_graph_editor, name='styled_graph_editor'),

    url(r'^create_combo/(?P<project_id>.+)/$', views.create_combo, name='create_combo'),
    url(r'^view_combo/(?P<project_id>.+)/(?P<combo_id>\d+)/$', views.view_combo, name='view_combo'),
    url(r'^save_combo_columns/(?P<project_id>.+)/(?P<combo_id>\d+)/$', views.save_combo_columns, name='save_combo_columns'),
    url(r'^delete_combo/(?P<project_id>.+)/(?P<combo_id>\d+)/$', views.delete_combo, name='delete_combo'),

    url(r'^combo/columns/(?P<project_id>.+)/(?P<combo_id>\d+)/$', views.combo_columns, name='combo_columns'),
    url(r'^combo/graphs/(?P<project_id>.+)/(?P<combo_id>\d+)/$', views.combo_graphs, name='combo_graphs'),
    url(r'^combo/view_grid(?P<project_id>.+)/(?P<combo_id>\d+)/$', views.combo_grid, name='combo_grid'),
    
    url(r'^combo/grid/handler/(?P<project_id>.+)/(?P<combo_id>\d+)/$', views.combo_grid_handler, name='combo_grid_handler'),
    url(r'^combo/grid/cfg/(?P<project_id>.+)/(?P<combo_id>\d+)/$', views.combo_grid_config, name='combo_grid_config'),
    url(r'^combo/grid/download/(?P<project_id>.+)/(?P<combo_id>\d+)/$', views.download_combo_dataframe, name='download_combo_dataframe'),

    url(r'^view_auto_combo/(?P<project_id>.+)/(?P<result_type>.+)/$', views.view_auto_combo, name='view_auto_combo'),
    url(r'^combo/auto/grid/handler/(?P<project_id>.+)/(?P<result_type>.+)/$', views.auto_combo_grid_handler, name='auto_combo_grid_handler'),
    url(r'^combo/auto/grid/cfg/(?P<project_id>.+)/(?P<result_type>.+)/$', views.auto_combo_grid_config, name='auto_combo_grid_config'),
    url(r'^combo/auto/grid/download/(?P<project_id>.+)/(?P<result_type>.+)/$', views.download_auto_combo_dataframe, name='download_auto_combo_dataframe'),

    url(r'^styled_graph_editor/editor_tab/(?P<project_id>.+)/(?P<styled_graph_id>\d+)/$', views.graph_editor_tab, name='graph_editor_tab'),
    url(r'^category/category_tab/(?P<project_id>.+)/(?P<styled_graph_id>\d+)/$', views.graph_category_tab, name='graph_category_tab'),
    url(r'^category/create/(?P<project_id>.+)/(?P<styled_graph_id>\d+)/$', views.create_category, name='create_category'),
    url(r'^category/view/(?P<project_id>.+)/(?P<category_id>\d+)/$', views.view_category, name='view_category'),
    url(r'^category/delete/(?P<project_id>.+)/(?P<category_id>\d+)/$', views.delete_category, name='delete_category'),

    url(r'^category/samples_tab/(?P<project_id>.+)/(?P<styled_graph_id>\d+)/$', views.graph_samples_tab, name='graph_samples_tab'),
    url(r'^category/view_samples/(?P<project_id>.+)/(?P<category_id>\d+)/$', views.view_graph_samples, name='view_graph_samples'),
    
    url(r'^lost_projects$', views.lost_projects, name='lost_projects'),
    url(r'^docs/(?P<doc_name>.+)/$', views.docs, name='docs'),

)