import HTSeq
from itertools import izip
import re

from bgs.utils.utils import Lazy
from reference_server import genomics
from reference_server.genomics import get_unique_features_from_genomic_array_of_sets_iv, \
    open_handle_gz, get_non_gzip_extension


REQUIRED_FIELDS = ['chrom', 'chrom_start', 'chrom_end']
OPTIONAL_FIELDS = ['name', 'score', 'strand']
THICK_ATTRIBUTES = ['thick_start', 'thick_end']
RGB_ATTRIBUTE = ['item_rgb']
BLOCK_ATTRIBUTES = ['block_count', 'block_sizes', 'block_starts']
OPTIONAL_ATTRIBUTES = THICK_ATTRIBUTES + RGB_ATTRIBUTE + BLOCK_ATTRIBUTES
ALL_OPTIONAL = OPTIONAL_FIELDS + OPTIONAL_ATTRIBUTES

PEAK_ATTRIBUTES = ['signalValue', 'pValue', 'qValue', 'peak']

TYPE_CONVERSIONS = {'chrom_start' : int,
                    'chrom_end': int,
                    'score' : float,
                    'thick_start' : int,
                    'thick_end' : int,
                    'block_count' : int,
                    'block_sizes' : str,
                    'block_starts' : str,
                    'signalValue' : float,
                    'pValue' : float,
                    'qValue' : float,
                    'peak' : int,}

BED_FILE = 'bedFile'
BED_DETAILS = 'bedDetail'
NARROW_PEAK = 'narrowPeak'
BROAD_PEAK = 'broadPeak'

BED_TYPES_REQUIRED_OPTIONAL = {BED_FILE : (REQUIRED_FIELDS, ALL_OPTIONAL),
                               BED_DETAILS : (REQUIRED_FIELDS, ALL_OPTIONAL),
                               NARROW_PEAK : (REQUIRED_FIELDS + OPTIONAL_FIELDS, PEAK_ATTRIBUTES),
                               BROAD_PEAK : (REQUIRED_FIELDS + OPTIONAL_FIELDS, PEAK_ATTRIBUTES[:3]),}

EXTENSIONS = {'.bed' : BED_FILE,
              '.narrowPeak' : NARROW_PEAK,
              '.broadPeak' : BROAD_PEAK,
}

class BedFileReader(object):
    '''
    Reads bed file
    Returns - iterator of HTSeq Genomic Features
    Attribute fields (column 7-12: 'thick_start', 'thick_end', 'block_count', 'block_sizes', 'block_starts')
        are saved as dict in feature attribute 'attr'. e.g. feature.attr['thick_start']
    '''
    def __init__(self, file_name, **kwargs):
        ''' bed_type = bedFile, bedDetail, narrowPeak, broadPeak or 'from_filename' (default=detect from header or bedFile) '''
        self.expected_genome = kwargs.get("expected_genome")
        self.source = open_handle_gz(file_name, "r")
        self.delimiter = kwargs.get("delimiter", '\t')
        self.want_chr = kwargs.get("want_chr")
        self.bed_type = kwargs.get('bed_type', BED_FILE)
        if self.bed_type == 'from_filename':
            ext = get_non_gzip_extension(file_name)
            bed_type = EXTENSIONS.get(ext)
            if bed_type:
                self.bed_type = bed_type
            else:
                msg = "bed_type from_filename unknown extensions: %s" % ext
                raise ValueError(msg) 
        
        self.read_header = False

    def __iter__(self):
        return self

    def next(self): #@ReservedAssignment
        line = self.source.next()
        # Not technically part of the spec, but skip blank lines
        while True:
            stripped = line.rstrip()
            if len(stripped) != 0:
                break
            else:
                line = self.source.next()
        

        while (not self.read_header):
            if line.startswith('track'):
                match = re.search("type=(.+?) ", line)
                if match:
                    bed_type = match.group(1)
                    if bed_type in BED_TYPES_REQUIRED_OPTIONAL:
                        self.bed_type = bed_type
                    else:
                        msg = "Uknown type='%s'" % bed_type
                        raise ValueError(msg)

                if self.expected_genome:
                    match = re.search("db=(.+?) ", line)
                    if match:
                        build = match.group(1)
                        assert build == self.expected_genome
            elif line.startswith('#') or line.startswith('browser'):
                pass
            else:
                self.read_header = True
                break
            
            line = self.source.next() # Keep reading

        
        columns = stripped.split(self.delimiter)
        details_columns = []
        if self.bed_type == BED_DETAILS:
            # Last 2 fields are details - strip them
            columns = columns[:-2]
            details_columns = columns[-2:] 

        (required, optional) = BED_TYPES_REQUIRED_OPTIONAL[self.bed_type]

        data = dict(zip(required, columns[:len(required)]))
        # Only put in optional fields when provided
        data.update(izip(optional, columns[len(required):]))
        # Convert to appropriate types
        for f in required + optional:
            if f in TYPE_CONVERSIONS and data.get(f):
                convert = TYPE_CONVERSIONS[f]
                data[f] = convert(data[f])

        # Make sure they aren't negative (some dodgy .BED files around)
        start = max(0, data["chrom_start"])
        end = max(0, data["chrom_end"])
        chrom = data["chrom"]
        if self.want_chr is not None: #format chrom
            chrom = genomics.format_chrom(chrom, self.want_chr)
        iv = HTSeq.GenomicInterval(chrom, start, end)
        name = data.get("name", "")
        score = data.get("score", 0.0) # Using "." here causes intron to not be displayed correctly
        iv.strand = data.get("strand", '.')
            
        feature = HTSeq.GenomicFeature(name, "from_bed", iv )
        feature.score = score
        feature.attr = {"score" : score} # So attr and .score are always the same (may be 0.0 if not found)

        # Copy set optional fields to GenomicFeature.attr
        for field in optional:
            value = data.get(field)
            if value:
                feature.attr[field] = value

        if details_columns:
            feature.attr["ID"] = details_columns[0]
            feature.attr["description"] = details_columns[1]
        
        return feature


class RandomAccessBedFileReader(object):
    ''' Like a BedFile, except it allows you to access it like:
    
        for feature in bed_reader[iv]:
            print feature
            
        Warning: This loads the entire bed file into ram!
    '''
    def __init__(self, file_name, **kwargs):
        self.stranded = kwargs.get("stranded", True)
        self.bed_file_reader = BedFileReader(file_name, **kwargs)

    @Lazy
    def genomic_array_of_sets(self):
        (genomic_array, _) = self.genomic_array_of_sets_and_length
        return genomic_array

    @Lazy
    def genomic_array_of_sets_and_length(self):
        '''Returns genomic array of sets and the number of features in the bed file'''
        genomic_array_of_features = HTSeq.GenomicArrayOfSets("auto", stranded=self.stranded)
        
        num_features = 0
        for feature in self.bed_file_reader:
            genomic_array_of_features[feature.iv] += feature
            num_features += 1
                    
        return (genomic_array_of_features, num_features)
    
    def __len__(self):
        (_, length) = self.genomic_array_of_sets_and_length
        return length
            
    def __getitem__(self, iv):
        ''' Returns (unordered) unique features in GenomicInterval iv '''
        if not self.stranded:
            iv = iv.copy()
            iv.strand = '.'
        
        unique_features = get_unique_features_from_genomic_array_of_sets_iv(self.genomic_array_of_sets, iv)
        for feature in unique_features:
            yield feature
    
    def __iter__(self):
        return iter(self.bed_file_reader)

