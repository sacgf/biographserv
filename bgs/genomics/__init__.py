import gzip
import os


def format_chrom(chrom, want_chr):
    ''' Pass in a chromosome (unknown format), return in your format
        @param chrom: chromosome ID (eg 1 or 'chr1')
        @param want_chr: Boolean - whether you want "chr" at the beginning of chrom
        @return: "chr1" or "1" (for want_chr True/False) 
    '''
    if want_chr:   
        if chrom.startswith("chr"):
            return chrom
        else:
            return "chr%s" % chrom
    else:
        return chrom.replace("chr", "")

def handle_open_file_gzip(filename, open_file):
    ''' If filename ends with .gz, remove .gz and open file as gzip '''
    if filename.endswith(".gz"):
        filename = os.path.splitext(filename)[0] # remove .gz
        open_file = gzip.GzipFile(fileobj=open_file)
    return (filename, open_file)