import os

from bgs.models import get_processing_dir, CSVResult, CSVResultType
import pandas as pd


class VCFPandasProcessor(object):
    def __init__(self, *args, **kwargs):
        super(VCFPandasProcessor, self).__init__(*args, **kwargs)
        self.project_id = kwargs.get("project_id")
        self.records = []
        
    ''' Converts a VCF into a pandas dataframe '''
    def set_reader(self, reader):
        self.reader = reader
        
    def process(self, v):
        for (alt_id, _) in enumerate(v.ALT):
            record = {"QUALITY" : v.QUAL}
            for (key, value) in v.INFO.iteritems():
                info = self.reader.infos[key]
                if info.num == 0: # Flag
                    value = True
                elif info.num == -1:
                    value = value[alt_id]
                elif info.num == -2: # Dont' handle genotype
                    continue
                record[key] = value
            self.records.append(record)

    def get_results(self, file_processing_task):
        print "get_results!"
        df = pd.DataFrame.from_records(self.records)

        working_dir = get_processing_dir(self.project_id)
        vcf_csv_filename = os.path.join(working_dir, 'vcf.csv')
        df.to_csv(vcf_csv_filename)
        vcf_df_result = CSVResult.objects.create(name='VCF_Info_counts',
                                                 file_processing_task=file_processing_task,
                                                 result_type=CSVResultType.VCF_INFO_COUNTS,
                                                 file_name=vcf_csv_filename)
        
        return [vcf_df_result]
