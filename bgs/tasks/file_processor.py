from celery.app.task import Task
from datetime import datetime
import logging

from bgs.models import ProcessingStatus, CSVResult, CSVResultType, \
    get_processing_dir, FileProcessingTask, Graph
from bgs.utils.utils import log_traceback


INITIAL_PROGRESS_STATUS = "Processing"

class AbstractFileProcessor(Task):
    abstract = True

    def start(self, file_processing_task):
        file_processing_task.status = ProcessingStatus.PROCESSING
        file_processing_task.start_date = datetime.now()
        file_processing_task.save()

    def success(self, file_processing_task, items_processed):
        file_processing_task.status = ProcessingStatus.SUCCESS
        #file_processing_task.items_processed = items_processed
        file_processing_task.end_date = datetime.now()
        file_processing_task.save()
    
    def error(self, file_processing_task):
        file_processing_task.status = ProcessingStatus.ERROR
        file_processing_task.end_date = datetime.now()
        file_processing_task.save()

    def process_items(self, file_processing_task):
        ''' returns (items_processed, results) '''
        pass

    def get_csv_result_types(self, file_type):
        return set()

    def run(self, file_processing_task_id):
        file_processing_task = FileProcessingTask.objects.get_subclass(pk=file_processing_task_id)
        # Call this first, so that celery makes the processing directory (before reference_server)
        project = file_processing_task.project
        processing_dir = get_processing_dir(project.pk)
        print "Processing dir: %s" % processing_dir

        try:
            self.start(file_processing_task)

            (items_processed, _) = self.process_items(file_processing_task)

            self.success(file_processing_task, items_processed)
        except:
            log_traceback()
            self.error(file_processing_task)
            

class FileProcessor(AbstractFileProcessor):
    abstract = True
    
    def get_file_reader(self, uploaded_file):
        return NotImplementedError()

    def get_processors_for_task(self, file_processing_task):
        kwargs = {'project_id' : file_processing_task.project.pk,
                  'base_name' : file_processing_task.get_base_name(),}
        
        if file_processing_task.uses_reference():
            reference = file_processing_task.get_reference()
            kwargs["reference"] = reference

        return self.get_processors(**kwargs)

    def get_csv_result_types(self, file_type):
        csv_result_types = set()
        if file_type in CSVResultType.UPLOADED_CSVRESULTS:
            csv_result_types.update(file_type)
        return csv_result_types
    
    def get_processors(self, **kwargs):
        return []

    def feed_processors(self, reader, processors):
        items_processed = 0
        for chunk in reader:
            for processor in processors:
                processor.process(chunk)
            
            items_processed += 1
        
        return items_processed

    def get_unprocessed_results(self, uploaded_file, file_processing_task):
        results = []
        if uploaded_file and uploaded_file.file_type in CSVResultType.UPLOADED_CSVRESULTS:
            csv_result = CSVResult.objects.create(name="", #"Uploaded File: %s" % uploaded_file.name,
                                                  file_processing_task=file_processing_task,
                                                  file_name=uploaded_file.uploaded_file.path,
                                                  result_type=uploaded_file.file_type)
            results = [csv_result]
        return results


    def process_items(self, file_processing_task):
        ''' returns (items_processed, results) '''

        print "Get reader"            
        self.file_processing_task = file_processing_task
        uploaded_file = file_processing_task.uploaded_file
        reader = self.get_file_reader(uploaded_file)

        items_processed = 0
        results = []
        results.extend(self.get_unprocessed_results(uploaded_file, file_processing_task))
        processors = self.get_processors_for_task(file_processing_task)

        if processors:
            for processor in processors:
                processor.set_reader(reader)

            print "Starting read"
            items_processed = self.feed_processors(reader, processors)

            print "Finished processing"
            for processor in processors:
                results.extend(processor.get_results(file_processing_task))

        return (items_processed, results)


    @staticmethod
    def get_result_of_type(results, result_type):
        for r in results:
            if r.result_type == result_type:
                return r
        msg = "Couldn't find result of type %s in results" % result_type
        raise ValueError(msg)

