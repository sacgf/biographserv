'''
Created on 5Dec.,2016

@author: dlawrence
'''

from collections import defaultdict
import os
import pandas as pd

from bgs.models import get_processing_dir, CSVResult, CSVResultType


class ChromosomeBinProcessor(object):
    def __init__(self, **kwargs):
        self.project_id = kwargs["project_id"]
        self.genomic_intervals = []

    def process(self, chunk):
        self.genomic_intervals.append(chunk.iv)

    def get_results(self, file_processing_task):
        chrom_xmin_xmax = defaultdict(lambda : ([], []))

        for iv in self.genomic_intervals:
            c = chrom_xmin_xmax[iv.chrom]
            c[0].append(iv.start)
            c[1].append(iv.end)
            
        working_dir = get_processing_dir(self.project_id)
        chrom_bins_csv_filename = os.path.join(working_dir, 'chrom_bin_counts.csv')
        chrom_bins_df = pd.DataFrame.from_dict(chrom_xmin_xmax)
        chrom_bins_df.to_csv(chrom_bins_csv_filename)
        
        bin_scores_csv = CSVResult.objects.create(name='Chrom Bin Scores',
                                                  file_processing_task=file_processing_task,
                                                  result_type=CSVResultType.CHROMOSOME_BIN_SCORES,
                                                  file_name=chrom_bins_csv_filename)
        
        return [bin_scores_csv]
        