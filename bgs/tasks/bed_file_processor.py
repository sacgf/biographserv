'''
Created on 25/11/2014

@author: dlawrence
'''

import json
import socket

from bgs.models import CSVResult, CSVResultType, Graph
from bgs.models_enum import GraphType
from bgs.tasks import file_processor
from bgs.tasks.bed.chromosome_bin_processor import ChromosomeBinProcessor
from biographserv import settings
from reference_server.genomics import chrom_data_access


class BedFileProcessor(file_processor.FileProcessor):
    def get_file_reader(self, uploaded_file):
        chrom_dao = chrom_data_access.ChromDAO(settings.CHROMOSOME_FORMAT_HAS_CHR)
        return chrom_dao.BED_Reader(uploaded_file.uploaded_file.path, bed_type='from_filename')
    
    def get_processors(self, **kwargs):
        return [ChromosomeBinProcessor(**kwargs)]
    
    
class ReferenceBedFileProcessor(file_processor.AbstractFileProcessor):
    def client(self, ip, port, message):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((ip, port))
        try:
            sock.sendall(json.dumps(message))
            response = sock.recv(2048)
            return json.loads(response)
        finally:
            sock.close()

    def get_csv_result_types(self, file_type):
        csv_result_types = super(ReferenceBedFileProcessor, self).get_csv_result_types(file_type)
        csv_result_types.update(set([CSVResultType.GENE_SCORES,
                                     CSVResultType.REGION_STATS,]))
        return csv_result_types

    def get_params(self, project):
        params = []
        for gr in project.genomicrange_set.all().values("name", "anchor", "upstream", "downstream"):
            params.append(gr)
        return params

    def process_items(self, file_processing_task):
        ''' returns (items_processed, results) '''

        igenomes_reference = file_processing_task.get_reference()
        print "reference = %s" % igenomes_reference
        project = file_processing_task.project
        file_name = file_processing_task.uploaded_file.uploaded_file.path

        data = {'reference_id' : igenomes_reference.get_reference_id(),
                'project_id' : project.pk,
                'file_name' : file_name,
                'file_type' : 'bed',
                'stranded' : False,
                'params' : self.get_params(project)
        }
        
        response = self.client(settings.REFERENCE_SERVER_HOSTNAME, settings.REFERENCE_SERVER_PORT, data)
        error_message = response.get("error")
        if error_message:
            raise RuntimeError(error_message)

        items_processed = 0 # TODO: Fill in via response
        gene_scores = response['gene_scores']
        regions_csv = response['regions']
        regions_result = CSVResult(name='Region Stats',
                                   result_type=CSVResultType.REGION_STATS,
                                   file_name=regions_csv),

        results = [regions_result]
        for gs in gene_scores:
            name = "Gene Scores (%s)" % gs["name"]
            result = CSVResult(name=name,
                               result_type=CSVResultType.GENE_SCORES,
                               file_name=gs["gene_scores_csv"])
            results.append(result)

        graph_result = Graph(name='Region Stats',
                             processing_result=regions_result,
                             graph_type=GraphType.BAR)
        results.append(graph_result)
        
        return (items_processed, results)

