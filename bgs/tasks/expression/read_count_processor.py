'''
Created on 07/12/2014

@author: dlawrence
'''
import os

from bgs.models import CSVResult, CSVResultType, get_processing_dir, Graph
from bgs.models_enum import GraphType


class ReadCountProcessor(object):
    normalized_input = False

    def __init__(self, **kwargs):
        self.base_name = kwargs["base_name"]
        self.project_id = kwargs["project_id"]
    
    def set_reader(self, df):
        pass

    def get_csv_result_types(self, file_type):
        return set([CSVResultType.READ_COUNTS,])
    
    def process(self, df):
        counts_df = df.sum()
        working_dir = get_processing_dir(self.project_id)
        self.counts_csv_filename = os.path.join(working_dir, '%s_read_counts.csv' % self.base_name)
        counts_df.to_csv(self.counts_csv_filename)


    def get_results(self, file_processing_task):
        counts_csv_result = CSVResult.objects.create(name='Read Counts',
                                                     file_processing_task=file_processing_task,
                                                     result_type=CSVResultType.READ_COUNTS,
                                                     file_name=self.counts_csv_filename)

        graph_result = Graph.objects.create(name='Read Counts',
                                            file_processing_task=file_processing_task,
                                            processing_result=counts_csv_result,
                                            graph_type=GraphType.BAR)
        
        return [counts_csv_result,
                graph_result]
