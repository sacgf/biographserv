'''
Created on 07/12/2014

@author: dlawrence
'''
import os

from bgs.models import CSVResult, CSVResultType, get_processing_dir, Graph
from bgs.models_enum import GraphType
from bgs.utils.stats_utils import correlation_dataframe


class CorrelationProcessor(object):
    normalized_input = False

    def __init__(self, **kwargs):
        self.base_name = kwargs["base_name"]
        self.project_id = kwargs["project_id"]
    
    def set_reader(self, df):
        pass

    def get_csv_result_types(self, file_type):
        return set([CSVResultType.OTHER,])
    
    def process(self, df):
        correlation_df = correlation_dataframe(df)
        # Sort rows and columns...
        correlation_df = correlation_df.sort_index(axis=0).sort_index(axis=1)
        
        working_dir = get_processing_dir(self.project_id)
        self.correlation_csv_filename = os.path.join(working_dir, '%s_correlations.csv' % self.base_name)
        correlation_df.to_csv(self.correlation_csv_filename)

    def get_results(self, file_processing_task):
        correlation_csv_result = CSVResult.objects.create(name='Correlations',
                                                          file_processing_task=file_processing_task,
                                                          result_type=CSVResultType.CORRELATION_MATRIX,
                                                          file_name=self.correlation_csv_filename)

        graph_result = Graph.objects.create(name='Correlation Matrix',
                                            file_processing_task=file_processing_task,
                                            processing_result=correlation_csv_result,
                                            graph_type=GraphType.CORRELATION_MATRIX)
        
        return [correlation_csv_result,
                graph_result]
