import os

from bgs.models import get_processing_dir, CSVResult, CSVResultType, Graph
from bgs.models_enum import GraphType
from bgs.utils.stats_utils import rename_common_suffix_columns
import numpy as np
import pandas as pd


no_rename_index = False
low_expression_cutoff = 3

class SVDProcessor(object):
    normalized_input = True
    
    def __init__(self, project_id):
        self.project_id = project_id

    def set_reader(self, df):
        pass
    
    def process(self, df):
        if not no_rename_index:
            df = rename_common_suffix_columns(df, df.columns)
        
        # Only use genes where at least one sample has a decent (log > 3) expression
        log_df = np.log(df + 0.01) # Add small amount so not inf log
        min_mask = log_df.max(axis=1) > low_expression_cutoff
        log_df = log_df[min_mask]

        U, s, Vt = np.linalg.svd(log_df, full_matrices=False)
        self.s = s[:15] # Don't want too much
        self.Vt = Vt
        self.columns = log_df.columns

    def get_results(self, file_processing_task):
        working_dir = get_processing_dir(self.project_id)
        scree_csv_filename = os.path.join(working_dir, 'svd_scree.csv')

        scree_data = {i+1 : v for i, v in enumerate(self.s)} # 1 based components
        scree_series = pd.Series(scree_data)
        scree_series.to_csv(scree_csv_filename)

        scree_result = CSVResult.objects.create(name='SVD Scree',
                                                file_processing_task=file_processing_task,
                                                result_type=CSVResultType.SVD_SCREE,
                                                file_name=scree_csv_filename)
        screen_graph = Graph.objects.create(name='SVD Scree Plot',
                                            file_processing_task=file_processing_task,
                                            processing_result=scree_result,
                                            graph_type=GraphType.LINE)

        svd_vt_csv_filename = os.path.join(working_dir, 'svd_vt.csv')
        vt_df = pd.DataFrame(data=self.Vt, columns=self.columns)
        vt_df.to_csv(svd_vt_csv_filename)
        svd_vt_result = CSVResult.objects.create(name='SVD VT',
                                                 file_processing_task=file_processing_task,
                                                 result_type=CSVResultType.SVD_VT,
                                                 file_name=svd_vt_csv_filename)

        svd_graph = Graph.objects.create(name='SVD VT',
                                         file_processing_task=file_processing_task,
                                         processing_result=svd_vt_result,
                                         graph_type=GraphType.TEXT_SCATTER)

        return [scree_result,
                screen_graph,
                svd_vt_result,
                svd_graph]
