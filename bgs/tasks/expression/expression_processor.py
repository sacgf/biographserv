'''
Created on 25/11/2014

@author: dlawrence
'''

from bgs.models import Graph
from bgs.models_enum import CSVResultType, GraphType
from bgs.tasks.expression.correlation_processor import CorrelationProcessor
from bgs.tasks.expression.read_count_processor import ReadCountProcessor
from bgs.tasks.expression.svd_processor import SVDProcessor
from bgs.tasks.file_processor import FileProcessor
from bgs.utils.pandas_utils import load_multi_column_df
from bgs.utils.stats_utils import dataframe_counts_to_rpm


class SingleSampleExpressionProcessor(FileProcessor):
    def get_unprocessed_results(self, uploaded_file, file_processing_task):
        results = super(FileProcessor).get_unprocessed_results(uploaded_file, file_processing_task)
        sample_expression_result = self.get_result_of_type(results, CSVResultType.SINGLE_SAMPLE_EXPRESSION)
        graph_result = Graph.objects.create(name='Expression Boxplot',
                                            file_processing_task=file_processing_task,
                                            processing_result=sample_expression_result,
                                            graph_type=GraphType.BOXPLOT)
        results.append(graph_result)
        return results



class MultiSampleExpressionProcessor(FileProcessor):
    def get_file_reader(self, uploaded_file):
        return load_multi_column_df(uploaded_file.uploaded_file.path)

    def get_processors(self, **kwargs):
        project_id = kwargs.get('project_id')
        return [SVDProcessor(project_id), CorrelationProcessor(**kwargs), ReadCountProcessor(**kwargs)]

    def feed_processors(self, df, processors):
        raw_processors = []
        normalized_processors = []

        for processor in processors:
            if processor.normalized_input:
                normalized_processors.append(processor)
            else:
                raw_processors.append(processor)

        for processor in raw_processors:
            processor.process(df)

        rpm_df = dataframe_counts_to_rpm(df)
            
        if normalized_processors:
            for processor in normalized_processors:
                processor.process(rpm_df)
            
        return len(df)

class ComboExpressionProcessor(MultiSampleExpressionProcessor):
    def get_file_reader(self, uploaded_file):
        combo = self.processing_task.combo
        return combo.get_dataframe()
