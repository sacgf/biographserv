'''
Created on 25/11/2014

@author: dlawrence
'''

from bgs.models import Graph
from bgs.models_enum import CSVResultType, GraphType
from bgs.tasks.file_processor import FileProcessor
import pandas as pd


class CuffdiffProcessor(FileProcessor):
    ''' At the moment we don't need to process this will just plot from unprocessed '''
    def get_file_reader(self, uploaded_file):
        return pd.read_table(uploaded_file.uploaded_file.path, sep='\t')

    def get_unprocessed_results(self, uploaded_file, file_processing_task):
        results = super(CuffdiffProcessor, self).get_unprocessed_results(uploaded_file, file_processing_task)
        cuffdiff_result = self.get_result_of_type(results, CSVResultType.CUFFDIFF)
        graph_result = Graph.objects.create(name='CuffDiff Volcano Plot',
                                            file_processing_task=file_processing_task,
                                            processing_result=cuffdiff_result,
                                            graph_type=GraphType.VOLCANO)
        results.append(graph_result)
        return results
